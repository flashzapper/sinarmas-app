import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
 
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { HTTP } from '@ionic-native/http';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Geolocation } from '@ionic-native/geolocation';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
 } from '@ionic-native/google-maps';

import { FirstPage } from '../pages/first/first';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { IntroPage } from '../pages/intro/intro';
import { ProfilePage } from '../pages/profile/profile';
import { DetailContentPage } from '../pages/detail-content/detail-content';
import { CreateActivityPage } from '../pages/create-activity/create-activity';
import { DetailEventPage } from '../pages/detail-event/detail-event';
import { CreateEventPage } from '../pages/create-event/create-event';
import { DetailProfilePage } from '../pages/detail-profile/detail-profile';
import { SettingPage } from '../pages/setting/setting';
import { LocationPage } from '../pages/location/location';
import { VerifiedUserPage } from '../pages/verified-user/verified-user';
import { MapsPage } from '../pages/maps/maps';
import { CommentPage } from '../pages/comment/comment';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { SearchLocationPage } from '../pages/search-location/search-location';
import { DonateSeedPage } from '../pages/donate-seed/donate-seed';
import { AddEventSponsorPage } from '../pages/add-eventsponsor/add-eventsponsor';
import { SearchTabsPage } from '../pages/search-tabs/search-tabs';
import { EventPopoverPage } from '../pages/event-popover/event-popover';

import { UserProvider } from '../providers/user/user';
import { ExampleProvider } from '../providers/example/example';
import { GlobalVarProvider } from '../providers/global-var/global-var';
import { AuthProvider } from '../providers/auth/auth';
import { RefProvider } from '../providers/ref/ref';
import { UtilityProvider } from '../providers/utility/utility';
import { ActivityProvider } from '../providers/activity/activity';
import { FileTransferProvider } from '../providers/file-transfer/file-transfer';
import { EventsProvider } from '../providers/events/events';
import { GoogleGeocodeProvider } from '../providers/google-geocode/google-geocode';
import { CommentProvider } from '../providers/comment/comment';
import { SearchProvider } from '../providers/search/search';

import { DetailEventParticipantsPage } from '../pages/detail-eventparticipants/detail-eventparticipants';
import { InboxPage } from '../pages/inbox/inbox';
import { InboxProvider } from '../providers/inbox/inbox';

@NgModule({
  declarations: [
    MyApp,
    FirstPage,
    TabsPage,
    LoginPage,
    IntroPage,
    RegisterPage,
    DetailContentPage,
    DetailEventPage,
    CreateActivityPage,
    CreateEventPage,
    DetailProfilePage,
    SettingPage,
    VerifiedUserPage,
    MapsPage,
    SearchLocationPage,
    EditProfilePage,
    CommentPage,
    SearchTabsPage,
    DonateSeedPage,
    AddEventSponsorPage,
    EventPopoverPage,
    DetailEventParticipantsPage,
    InboxPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FirstPage,
    TabsPage,
    LoginPage,
    IntroPage,
    RegisterPage,
    DetailContentPage,
    DetailEventPage,
    CreateEventPage,
    CreateActivityPage,
    SettingPage,
    DetailProfilePage,
    VerifiedUserPage,
    MapsPage,
    EditProfilePage,
    SearchLocationPage,
    CommentPage,
    SearchTabsPage,
    DonateSeedPage,
    AddEventSponsorPage,
    EventPopoverPage,
    DetailEventParticipantsPage,
    InboxPage
  ],
  providers: [
    File,
    Transfer,
    Camera,
    FilePath,
    HTTP,
    SocialSharing,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    ExampleProvider,
    GlobalVarProvider,
    AuthProvider,
    RefProvider,
    UtilityProvider,
    ActivityProvider,
    FileTransferProvider,
    EventsProvider,
    GoogleGeocodeProvider,
    GoogleMaps,
    Geolocation,
    CommentProvider,
    SearchProvider,
    InboxProvider
  ],
})
export class AppModule {}