import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , FabContainer , ModalController } from 'ionic-angular';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { ActivityProvider } from '../../providers/activity/activity';
import { UtilityProvider } from '../../providers/utility/utility';
import { Observable } from 'rxjs/Observable';

import { SearchTabsPage } from '../search-tabs/search-tabs';
import { DetailContentPage } from '../detail-content/detail-content';
import { CommentPage } from '../comment/comment';
import { CreateActivityPage } from '../create-activity/create-activity';



@IonicPage()
@Component({
  selector: 'page-activity',
  templateUrl: 'activity.html',
})



export class ActivityPage {
  obsData : Observable<any>;
  data : any;
  limit : number = 10;
  offset : number = 0;
  canLoadMore : boolean = true;
  loginState : boolean;
  selectedTab : string;

  constructor(public navCtrl: NavController, public navParams: NavParams , public globalVar : GlobalVarProvider , public activity : ActivityProvider , public util : UtilityProvider , public modal : ModalController) {
    this.selectedTab = "ALL";
    this.loginState = this.globalVar.loginState;
  }

  onSearchTabChange($event)
  {
    
    this.selectedTab = $event.value;
    console.log(this.selectedTab);
    this.getActivity("refresh",null);
  }

  getActivity(isMore : string , refOrInf : any) {
    this.limit = this.limit;
    this.offset = this.offset + this.limit;

    if(isMore == "refresh") 
    {
      this.limit = 10;
      this.offset = 0;
      this.canLoadMore = true;
    }

      
      this.obsData = this.activity.getActivity(this.limit,this.offset,this.selectedTab);
      this.obsData.subscribe(response => {
          if(response.total_rows > 0)
          {
            this.processData(response,isMore,refOrInf);
          }
          else
          {
            this.data = response.data;
            this.util.showToast(response.message);
            if(refOrInf)
              {
                refOrInf.complete();
              }		

          }    
      },error => {
        if(refOrInf)
        {
          refOrInf.complete();
        }	
        this.util.showToast(error.message);
      }); 
    
  }

  processData(response:any , isMore:string, refOrInf : any) {
    if(response.status == 1)
      {		
        if(isMore == 'more')
        {
          console.log('more');
          if(response.data.length <= 0)
          {
            this.canLoadMore = false;
            console.log('cant load more');
            if(refOrInf)
            {
              refOrInf.complete();
            }	
            
          }
          else
          {
            this.canLoadMore = true;
            this.data = this.data.concat(response.data);
            if(refOrInf)
            {
              refOrInf.complete();
            }	
          }
          
        }
        else
        {	
          this.data = response.data;
          if(refOrInf)
            {
              refOrInf.complete();
            }			
        }              
      }
      else
      {
        this.data = response.data;
        this.util.showToast(response.message);
        if(refOrInf)
          {
            refOrInf.complete();
          }		
                    
      }
  }

  doRefresh(refresher) {
    this.getActivity("refresh",refresher);
  }

  doInfinite(infinite) {
    console.log(infinite);
    this.getActivity("more",infinite);
  }

  goDetail(id) {
    this.navCtrl.push(DetailContentPage,{id : id, myProfile : false});
  }

  createActivity(fab : FabContainer) {
    fab.close();
    this.navCtrl.push(CreateActivityPage);
  }

  navToComments(id) {
    //this.navCtrl.push(CommentPage,{id : id});
    let modal = this.modal.create(CommentPage,{id : id});
    modal.onDidDismiss(data => {
      //callback when dismiss
    });
    modal.present();
  }

  

  ionViewDidLoad() {
   
  }

  ionViewWillEnter() {
  
    console.log(this.data);
    if(this.data == null)
    {
      this.getActivity("refresh",null);
    }

    if(!this.globalVar.loginState)
    {
      setTimeout(() => {
        this.util.alertGuest("a",null);
       }, 2000);
    }
  }

  goToSearchPage()
  {
     this.navCtrl.push(SearchTabsPage);
  }
}
