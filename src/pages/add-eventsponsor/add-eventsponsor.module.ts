import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEventSponsorPage } from './add-eventsponsor';

@NgModule({
  declarations: [
    AddEventSponsorPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEventSponsorPage),
  ],
})
export class AddEventSponsorPageModule {}
