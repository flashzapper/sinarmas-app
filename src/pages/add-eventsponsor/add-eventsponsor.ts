import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, AlertController, ActionSheetController, Platform ,Alert} from 'ionic-angular';

//////////////// for browse photo
import { File } from '@ionic-native/file';
import { Transfer ,TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera , CameraOptions } from '@ionic-native/camera';


import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';

declare var cordova: any;
declare var window;
/**
 * Generated class for the AddEventsponsorPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-eventsponsor',
  templateUrl: 'add-eventsponsor.html',
})
export class AddEventSponsorPage {
  selectedSponsors : Array<any>;
  noSponsor : boolean;
  sponsorManualList : Array<any>;
  sponsorManualMode : any;
  eventSponsorNames : Array<any>;
  tempSponsorManualList : Array<any>;
  size : any;
  isTooLarge : boolean = false;

  constructor(public viewCtrl: ViewController, /*public navCtrl: NavController,*/ public navParams: NavParams, public globalVar : GlobalVarProvider, public util : UtilityProvider, public alert : AlertController, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController,  public platform: Platform) {
    this.selectedSponsors = navParams.get('selectedSponsors'); 
    this.noSponsor = navParams.get('noSponsor');      
    this.sponsorManualMode = navParams.get('sponsorManualMode');
    this.sponsorManualList = [];    
    this.eventSponsorNames = [];
    this.tempSponsorManualList = navParams.get('sponsorManualList');    

    console.log(this.noSponsor);
    console.log(this.sponsorManualList);
    console.log(this.sponsorManualMode);
  }

  ionViewDidLoad() {
  
    this.tempSponsorManualList.forEach(sponsorManual => {
      // if (this.sponsorManualMode.checked && !this.sponsorManualMode.disabled)
      // {
      this.sponsorManualList.push({ name: sponsorManual.name, logo: sponsorManual.logo });
      // }  
    });
    
    console.log('ionViewDidLoad AddEventSponsorPage');    
    console.log(this.sponsorManualList);
  }

  toggleDisableAllSponsors()
  {    
    this.selectedSponsors.forEach(sponsor => {
      sponsor.checked = false;
      sponsor.disabled = this.noSponsor;
    });

    if (this.noSponsor)
    {
      this.sponsorManualMode.checked = false;
    }

    this.sponsorManualMode.disabled = this.noSponsor;
  }

  saveSponsorData()
  {
    let newSelectedSponsorIds = this.extractSelectedSponsorIds();
    let newSelectedSponsorManualList = this.extractSelectedSponsorManualList();

    console.log(newSelectedSponsorIds);
    console.log(this.noSponsor);
    console.log(newSelectedSponsorManualList);

    this.sponsorManualList.forEach(sponsorManual => {
    if(sponsorManual.isTooLarge)
    {
      this.isTooLarge = true;
      return;
    }
  });

    if(!this.isTooLarge)
    {
      let data = {
        selectedSponsors : this.selectedSponsors, 
        selectedSponsorIds : newSelectedSponsorIds, 
        noSponsor : this.noSponsor,
        sponsorManualMode : this.sponsorManualMode,
        sponsorManualList : newSelectedSponsorManualList,
        eventSponsorNames : this.eventSponsorNames
      };
      this.viewCtrl.dismiss(data);
    }
    else
    {
      this.util.showAlertImageTooLarge();
    }
   
    
    
  }

  extractSelectedSponsorIds()
  {
    let sponsorIds = [];
    this.selectedSponsors.forEach(sponsor => {
      if (sponsor.checked)
      {
        sponsorIds.push(sponsor.id);
        this.eventSponsorNames.push(sponsor.name);
      }  
    });

    console.log(sponsorIds);

    return sponsorIds;
  }

  extractSelectedSponsorManualList()
  {
    let extractedSponsorManualList = [];
    if (!this.sponsorManualMode.disabled)
    {
      this.sponsorManualList.forEach(sponsorManual => {
       
         

        if (sponsorManual.name != "")
        {
          extractedSponsorManualList.push(sponsorManual);
          this.eventSponsorNames.push(sponsorManual.name);
        }
      });
    } 
    console.log(extractedSponsorManualList);

    return extractedSponsorManualList;
  }

  //////////////////////////////////////////
  // for handling multiple upload sponsor logo
  addNewSponsorManual()
  {
    if (this.sponsorManualMode.checked && !this.sponsorManualMode.disabled)
    {
      this.sponsorManualList.push({ name: "", logo: null  , isTooLarge : false    
        });
            
    }
    else
    {
      this.util.showToast('Pilih Sponsor Manual terlebih dahulu.');
    }
  }

  browseImage(sponsorManual)
  {
    if (this.sponsorManualMode.checked && !this.sponsorManualMode.disabled)
    {
      this.presentActionSheet(sponsorManual);
    }
  }

  public presentActionSheet(sponsorManual) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePictureTo(sponsorManual, this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        // {
        //   text: 'Use Camera',
        //   handler: () => {
        //     this.takePictureTo(sponsorManual, this.camera.PictureSourceType.CAMERA);
        //   }
        // },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public getCameraOptions(sourceType) {
    let options : CameraOptions  = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit : true,      
    };
   
    return options;
  }

  public takePictureTo(sponsorManual, sourceType) {
    // Create options for the Camera Dialog
    let options = this.getCameraOptions(sourceType);
    // Get the data of an image
 
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(sponsorManual, correctPath, currentName, this.util.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(sponsorManual, correctPath, currentName, this.util.createFileName());
      }
    }, (err) => {
      this.util.showToast('Error while selecting image.');
    });
  }

  public copyFileToLocalDir(sponsorManual, namePath, currentName, newFileName) {    
      //-- uncomment when deploying to device --//    
      this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
        sponsorManual.logo = newFileName;
        this.getFileSize(sponsorManual,namePath,currentName,newFileName);
      }, error => {
        this.util.showToast('Error while storing file.');
      });
  }
  /////////////////////////////////////////

  public getFileSize(sponsorManual,namePath,currentName,newFileName) {
    let currentSize;
    window.resolveLocalFileSystemURL(namePath, 
      function (fileSystem) {
          fileSystem.getFile(currentName, {create: false}, 
              function (fileEntry) {
                  fileEntry.getMetadata(
                      function (metadata) {
                          console.log("METADATA",metadata); // get file size
                          currentSize = metadata.size;
                          if(metadata.size > 4098000)  // diatas 4mb berarti kegedean
                          { 
                             sponsorManual.isTooLarge = true;
                          }                 
                        }, 
                      function (error) {}
                  );
              }, 
              function (error) {}
          );
      }, 
      function (error) {} 
  );     
    console.log(this.sponsorManualList);
  }

  public checkLargeImage(sponsorManualList) {
    sponsorManualList.forEach(item => {
      if(item.isTooLarge)
      {
        this.isTooLarge = true;
      }
    });
  }

}