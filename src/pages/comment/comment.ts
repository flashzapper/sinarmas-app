import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { CommentProvider } from '../../providers/comment/comment';
import { UtilityProvider } from '../../providers/utility/utility';


@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {
  
  
  obsData : Observable<any>;
  data : any;
  limit : number = 10;
  offset : number = 0;
  canLoadMore : boolean = true;
  loginState : boolean;
  activity_id : string;
  comment : string;



  constructor(public navCtrl: NavController,  public navParams: NavParams , public glovalVar : GlobalVarProvider , public Comment : CommentProvider , public util : UtilityProvider) {
    this.activity_id = this.navParams.get("id");
  }

  postComments() {
    if(this.glovalVar.loginState)
    {
      let postData = {};
      postData['comment'] = this.comment;
      postData['id_activity'] = this.activity_id;

      console.log("POST DATA ",postData);
      let loading = this.util.showLoading("Posting comment..")
      loading.present();

      this.obsData = this.Comment.postComment(postData);
      this.obsData.subscribe(response => {
          
        console.log("Response",response);
        let appendData = {};
        appendData = response.data;
        appendData['avatar'] = this.glovalVar.myProfile.avatar;
        appendData['fullname'] = this.glovalVar.myProfile.fullname;
        this.data.push(appendData);
        console.log(this.data);
        loading.dismissAll();
        this.comment = null; 

      },error => {
        this.util.showToast(error);
      }); 
    }
    else
    {
      this.util.showToast("Login untuk komentar.");
    }
  }

  getComments(id : any , isMore : string , refOrInf : any) {
    this.limit = this.limit;
    this.offset = this.offset + this.limit;

    if(isMore == "refresh") 
    {
      this.limit = 10;
      this.offset = 0;
      this.canLoadMore = true;
    }

    this.obsData = this.Comment.getComment(id,this.limit,this.offset);
    this.obsData.subscribe(response => {
      this.processData(response,isMore,refOrInf);
    },error => {
      if(refOrInf)
      {
         refOrInf.complete();
      }
      this.util.showToast(error.message);
    }); 
  }

  processData(response:any , isMore:string, refOrInf : any) {
    if(response.status == 1)
      {		
        if(isMore == 'more')
        {
          console.log('more');
          if(response.data.length <= 0)
          {
            this.canLoadMore = false;
            console.log('cant load more');
            
          }
          else
          {
            this.canLoadMore = true;
            this.data = this.data.concat(response.data);
            if(refOrInf)
            {
              refOrInf.complete();
            }	
          }
          
        }
        else
        {	
          this.data = response.data;
          if(refOrInf)
          {
            refOrInf.complete();
          }			
        }              
      }
      else
      {
        this.util.showToast(response.data.message);  
        if(refOrInf)
          {
            refOrInf.complete();
          }		
                    
      }
  }

  doRefresh(refresher) {
    this.getComments(this.activity_id,"refresh",refresher);
  }

  doInfinite(infinite) {
    console.log(infinite);
    this.getComments(this.activity_id,"more",infinite);
  }

  goBack() {
    this.navCtrl.pop();
  }

  ionViewWillEnter() {
      this.getComments(this.activity_id,"refresh",null);
  }
 
  ionViewWillLeave() {
   
  }

  checkAvatar(avatar) {
      if(avatar)
      {
        let url = "";
        return url + avatar;
      }
      else
      {
        return "assets/img/user-placeholder.png";
      }
  }

}
