import { Component } from '@angular/core';
import { IonicPage ,NavController, ActionSheetController, ModalController, Platform} from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Transfer ,TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera , CameraOptions } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';
import { RefProvider } from '../../providers/ref/ref';
import { GoogleGeocodeProvider } from '../../providers/google-geocode/google-geocode';

import moment from 'moment';

import { SearchLocationPage } from '../search-location/search-location';

import { Observable } from 'rxjs/Observable';


declare var cordova: any;
declare var window: any;
//var cordova;

@IonicPage()
@Component({
  selector: 'page-create-activity',
  templateUrl: 'create-activity.html',
})
export class CreateActivityPage {

  lastImage : string = "assets/img/default-placeholder.png";
  firstLoad : boolean = true;
  location : string;
  caption : string;
  deskripsi : string;
  lat : string;
  lng : string;
  trees : any;
  events : any;
  obsData : Observable<any>;
  obsData2 : Observable<any>;
  obsData3 : Observable<any>;
  eventsData : any;
  treesData : any;
  size : any;
  isTooLarge : boolean = false;
  


  constructor(public navCtrl: NavController, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public util : UtilityProvider , public platform: Platform , public globalVar : GlobalVarProvider , public ref : RefProvider , public modal : ModalController ,private geolocation: Geolocation ,public gmaps : GoogleGeocodeProvider) {

    this.getEventsData();
    this.getTreesData();
    this.getApproximatePosition();
   }

   public checkMandatory() {
     if(!this.location || !this.caption || !this.trees || !this.events )
      return false;
   }


  public getApproximatePosition() {
    this.geolocation.getCurrentPosition().then((resp) => {
       // resp.coords.latitude
      // resp.coords.longitude
      this.obsData = this.gmaps.searchLocationByLatLng(resp.coords.latitude,resp.coords.longitude);
      this.obsData.subscribe(data => {
        console.log("RESULT", data.results);
        if(data.results[0])
        {
          this.location = data.results[0].formatted_address;
          this.lat = data.results[0].geometry.location.lat;
          this.lng = data.results[0].geometry.location.lng;
        }
    
        },error => {
           console.log("ERROR",error);
       }); 
     


     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }
  public showLocationModal() {
    let modal = this.modal.create(SearchLocationPage);
    modal.onDidDismiss(data => {
      console.log(data);
      if(data)
        {
          this.location = data.location;
          this.lat = data.lat;
          this.lng = data.lng;
        }
    });
    modal.present();
  }
  
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public getEventsData() {
    this.obsData = this.ref.events();
    this.obsData.subscribe(response => {
      this.eventsData =  response.data;
    //   let noData = {'id' : 0 , 'event_name' : "No Event"};
    //   this.eventsData.unshift(noData);
     },error => {
       this.util.showToast(error);
    }); 
  }

  public getTreesData() {
    this.obsData2 = this.ref.trees();
    this.obsData2.subscribe(response => {
      this.treesData =  response.data;
      // let noData = {'id' : 0 , 'tree_name' : "Jenis Pohon Lainnya"};
      // this.treesData.push(noData);
    },error => {
       this.util.showToast(error);
    }); 
  }

  public getCameraOptions(sourceType) {
    let options : CameraOptions  = {
      targetWidth: 800,
      targetHeight: 800,
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit : true,
      
    };
   
    return options;
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    let options = this.getCameraOptions(sourceType);
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
       // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.util.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.util.createFileName());
      }
    }, (err) => {
      this.util.showToast('Error while selecting image.');
    });
  }

  // Copy the image to a local folder
  public copyFileToLocalDir(namePath, currentName, newFileName) {

    //-- uncomment when deploying to device --//

    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      //  this.getFileSize(namePath,currentName,newFileName);
        this.firstLoad = false;
        this.lastImage = newFileName;     
    }, error => {
      this.util.showToast('Error while storing file.');
    });
}


 
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
      //-- uncomment when deploying to device --//
      if(this.firstLoad) 
        {
            return img;
        }
        else
        {
          return cordova.file.dataDirectory + img;
        }   
   // return img;
  }
}


public doCreateActivity() {
  
  // Destination URL
  var url = this.globalVar.urlBase+"/posting";
  // File for Upload
  var targetPath = this.pathForImage(this.lastImage);
  // File name only
  var filename = this.lastImage;
 
   let paramData = {}; 
  paramData['image'] = filename;
  paramData['location'] = this.location;
  paramData['deskripsi'] = this.deskripsi;
  paramData['caption'] = this.caption;
  paramData['tree_id'] = this.trees;
  paramData['event_id'] = this.events;
  paramData['lat'] = this.lat;
  paramData['lng'] = this.lng;
  paramData['profiles_id'] = this.globalVar.myProfile.user_id;
  paramData['date_plant'] = moment(new Date()).format("YYYY-MM-DD");
  


  var options = {
    fileKey: "image",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : paramData // postData here
  };
  
  options.params['headers'] = { Authorization : "Bearer "+this.globalVar.token};

  console.log(options);

  const fileTransfer: TransferObject = this.transfer.create();
 
  let loading = this.util.showLoading("Posting activity...")
  loading.present();
 
  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, url, options).then(data => {
      loading.dismissAll()
      console.log("SUKSES",data);
      this.util.showToast("Sukses menyimpan data.");
      this.navCtrl.pop();
  }, error => {
      loading.dismissAll()
      console.log("ERROR",error);   
      this.util.showToast(error);
  });
  }
}
