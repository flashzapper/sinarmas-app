import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController ,AlertController} from 'ionic-angular';
import { SearchLocationPage } from '../search-location/search-location';
import { AddEventSponsorPage } from '../add-eventsponsor/add-eventsponsor';

import { RefProvider } from '../../providers/ref/ref';
import { EventsProvider } from '../../providers/events/events';
import { UtilityProvider } from '../../providers/utility/utility';

import moment from 'moment';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the CreateEventPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-event',
  templateUrl: 'create-event.html',
})
export class CreateEventPage {
  location : string;
  lat : string;
  lng : string;
  desc : string;
  sponsor : any; // selected sponsor
  event_date : string;
  event_name : string;
  event_time : string;
  seed_range : any;
  area_range : any;

  isSubmitting : boolean = false;

  seedRangeList : any;
  areaRangeList : any; 
  obsData : Observable<any>;
  sponsorData : Array<any>;
  today : string = new Date().toISOString();

  selectedSponsors : Array<any>;
  selectedSponsorIds : Array<any>;
  noSponsor : boolean;
  sponsorManualMode : any;
  sponsorManualList : Array<any>;
  eventSponsorNames : Array<any>;
  eventSponsorName : string;;

  constructor(public navCtrl: NavController, public navParams: NavParams , public modal : ModalController , public ref : RefProvider , public util : UtilityProvider , public events : EventsProvider , public alert : AlertController) {
    this.getSponsor();
    console.log(this.seedRangeList);
    console.log(this.areaRangeList);
    this.seedRangeList = [{ value:500, description:"<500 Bibit" }, { value:1000, description:"500-1000 Bibit" }, { value:-1, description:">1000 Bibit" }];
    this.areaRangeList = [{ value:1, description:"<1 Hektar" }, { value:10, description:"1-10 Hektar" }, { value:-1, description:">10 Hektar" }]; 
    this.selectedSponsors = [];  
    this.selectedSponsorIds = [];
    this.noSponsor = false;
    this.sponsorManualMode = { checked : false, disabled : false };
    this.sponsorManualList = [];
    this.eventSponsorNames = [];
    this.event_date = new Date().toISOString();
  }

   pickLokasi() {
    let modal = this.modal.create(SearchLocationPage);
    modal.onDidDismiss(data => {
      console.log(data);
      if(data)
        {
          this.location = data.location;
          this.lat = data.lat;
          this.lng = data.lng;
        }
    });
    modal.present();
  }

  addEventSponsor()
  {
    let modal = this.modal.create(AddEventSponsorPage, 
      {
        selectedSponsors : this.selectedSponsors,
        noSponsor : this.noSponsor,
        sponsorManualMode : this.sponsorManualMode,
        sponsorManualList : this.sponsorManualList
      });
    modal.onDidDismiss(data => {
      console.log(data);
      if (data)
      {
        this.selectedSponsors = data.selectedSponsors;
        this.selectedSponsorIds = data.selectedSponsorIds; 
        this.noSponsor = data.noSponsor;
        this.sponsorManualMode = data.sponsorManualMode;
        this.sponsorManualList = data.sponsorManualList;
        this.eventSponsorNames = data.eventSponsorNames;
        this.eventSponsorName = "";
        if (this.noSponsor)
        {
          this.eventSponsorName = "No Sponsor";
        }
        else
        {
          let i = 0;
          for (let index in this.eventSponsorNames)
          {
            let delimiter = (i === 0) ? "" : ", ";
            this.eventSponsorName += delimiter + this.eventSponsorNames[index];
            ++i;
          }
        }        
      }
    });

    modal.present();
  }

  submitEvent() {
    this.isSubmitting = true;
    let loading = this.util.showLoading("Sedang memproses data...")
    loading.present();

    let newSponsorManualList =  [];    
    console.log("trying to upload image");
    let i = 0;
    if (this.sponsorManualList.length > 0)
    {
      this.sponsorManualList.forEach(sponsorManual => 
      {
        // upload image first
        let imageFileName = sponsorManual.logo;
        console.log("logo: " + imageFileName);
        this.ref.uploadImage(imageFileName).then(result => {
          loading.dismissAll();
            console.log("Sukses mengunggah gambar", result);
            let apiResponse = JSON.parse(result.response);
            if (apiResponse.data)
            {
              let resultImageURL = apiResponse.data.file_name;
              console.log("url: " + resultImageURL + " logo: " + sponsorManual.logo);
              newSponsorManualList.push({ name : sponsorManual.name, logo : resultImageURL });
            }

            if (newSponsorManualList.length == this.sponsorManualList.length)
            {
              this.submitEventData(newSponsorManualList,loading);
            }
        }, error => {
            loading.dismissAll();
            console.log("ERROR", "Gagal mengunggah gambar", error); 
            // this.util.showToast("Gagal mengunggah gambar");            
            newSponsorManualList.push({ name : sponsorManual.name, logo : null });
            if (newSponsorManualList.length == this.sponsorManualList.length)
            {
              this.submitEventData(newSponsorManualList,loading);
            }
           
        });
      });  
    }
    else
    {
      this.submitEventData(this.sponsorManualList,loading);      
    }      
  }

  submitEventData(sponsorManualListData,loading)
  {
    this.isSubmitting = true;

    let new_event_date = moment(new Date(this.event_date)).format("YYYY-MM-DD");
    

    let extractedSponsorManualList = [];
    sponsorManualListData.forEach(sponsorManual => {
      if (sponsorManual.name != "")
      {
        extractedSponsorManualList.push(sponsorManual);
      }
    });

    let postData = {};
    console.log("inserting postData");   
    console.log("new_event_date: " + new_event_date);   
    postData['event_name'] = this.event_name;
    postData['event_date'] = new_event_date;
    postData['event_time'] = this.event_time;
    postData['seed_range'] = this.seed_range;
    postData['area_range'] = this.area_range;
    postData['location'] = this.location;    
    postData['sponsor'] = {
      sponsor_ids : this.selectedSponsorIds,
      no_sponsor : this.noSponsor,
      sponsor_manual : extractedSponsorManualList
    };
    postData['lat'] = this.lat;
    postData['lng'] = this.lng;
    postData['desc'] = this.desc;

    console.log("POST EVENT DATA ",postData);

    this.obsData = this.events.createEvents(postData);
    this.obsData.subscribe(response => {
       this.isSubmitting = true;
       loading.dismissAll();
       console.log("Response",response);
       this.showPopUp();
    },error => {
      loading.dismissAll();
      this.isSubmitting = true;
    
       this.util.showToast(error);
    });
  }

  getSponsor() {
    this.obsData = this.ref.sponsor();
    this.obsData.subscribe(response => {
      this.sponsorData =  response.data;
      console.log(this.sponsorData);
      this.sponsorData.forEach(sponsor => {
        this.selectedSponsors.push(
          { 
            id : sponsor.id, 
            name : sponsor.sponsors_name,
            checked : false, 
            disabled : false
          });  
      });
    },error => {
        this.util.showToast(error);
    }); 
  }

  showPopUp() {
    let alert = this.alert.create({
      title: 'Sukses',
      subTitle: 'Pengajuan event berhasil , event akan dipublish jika sudah disetujui Admin.',
      buttons: [{
        text: 'OK',
        handler: () => {
          console.log('Clicked OK');
          this.navCtrl.pop();
        }
      }]
    });
    alert.present();
  }

  ionViewDidLoad() {
    
  }
}
