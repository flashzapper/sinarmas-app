import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailContentPage } from './detail-content';

@NgModule({
  declarations: [
    DetailContentPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailContentPage),
  ],
})
export class DetailContentPageModule {}
