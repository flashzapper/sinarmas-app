import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ModalController } from 'ionic-angular';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { ActivityProvider } from '../../providers/activity/activity';
import { UtilityProvider } from '../../providers/utility/utility';
import { CommentProvider } from '../../providers/comment/comment';
import { Observable } from 'rxjs/Observable';

import { DetailProfilePage } from '../detail-profile/detail-profile';
import { MapsPage } from '../maps/maps';
import { CommentPage } from '../comment/comment';

/**
 * Generated class for the DetailContentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-content',
  templateUrl: 'detail-content.html',
})
export class DetailContentPage {
  activity_id : number;
  obsData : Observable<any>;
  data : any;
  isMyProfile : boolean;

  limit : number = 10;
  offset : number = 0;
  obsCommentData : Observable<any>;
  commentData : any;
  commentCount : any;

  constructor(public navCtrl: NavController, public navParams: NavParams , public globalVar : GlobalVarProvider , public util : UtilityProvider , public activity : ActivityProvider, public modal : ModalController , public Comment : CommentProvider) {
    this.activity_id = this.navParams.get('id');
    this.isMyProfile = this.navParams.get('myProfile');
    console.log('is my profile ? :', this.isMyProfile);
    console.log('navParam :', this.activity_id);
  }
  
  getDetailActivity() {
    this.obsData = this.activity.detailActivity(this.activity_id);
    this.obsData.subscribe(response => {
        console.log("detail activity",response.data);
        this.data = response.data;
        this.getComments(this.activity_id);
    },error => {
      this.util.showToast(error);
    })
  }

  goToOtherProfile(id : any) {
    if(!this.globalVar.loginState)
    {
      console.log(id);
      this.navCtrl.push(DetailProfilePage,{id : id})
    }
    else
    {
      if(!this.isMyProfile && (id != this.globalVar.myProfile.user_id) )
      {
        console.log(id);
        this.navCtrl.push(DetailProfilePage,{id : id})
      }
    }
      
     
  }

  getComments(id : any) {
    this.limit = 4;
    this.offset = 0;

    this.obsCommentData = this.Comment.getComment(id,this.limit,this.offset);
    this.obsCommentData.subscribe(response => {
      this.processData(response);
    },error => {
      this.util.showToast(error.message);
    }); 
  }

  processData(response:any) {
    if(response.status == 1)
    {		
        this.commentData = response.data;
        this.commentCount = response.total_rows;			
    }
    else
    {
      this.util.showToast(response.data.message);         
    }
  }

  ionViewWillEnter() {
    console.log("entered page");
    if(this.data == null)
    {
      this.getDetailActivity(); 
    }

    this.getComments(this.activity_id);

    if(!this.globalVar.loginState)
    {
      setTimeout(() => {
        this.util.alertGuest("dc",{id : this.activity_id , myProfile : this.isMyProfile});
        }, 2000);
    }
      
  }

  navToComments(id) {
    let modal = this.modal.create(CommentPage,{id : id});
    modal.onDidDismiss(data => {
      //callback when dismiss
    });
    modal.present();
  }

  showMap() {
    this.navCtrl.push(MapsPage , {lat : this.data.lat , lng : this.data.lng});
  }

   ionViewDidLoad() {
   }

}
