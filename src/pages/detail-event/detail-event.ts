import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';
import { EventsProvider } from '../../providers/events/events';
import { MapsPage } from '../maps/maps';
import { DonateSeedPage } from '../donate-seed/donate-seed';
import { DetailEventParticipantsPage } from '../detail-eventparticipants/detail-eventparticipants';

import { Observable } from 'rxjs/Observable';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-detail-event',
  templateUrl: 'detail-event.html',
})
 
export class DetailEventPage {

  obsData : Observable<any>;
  data : any;
  buttonLoading : boolean = false;
  event_id : number;
  participantData : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar : GlobalVarProvider, private socialSharing: SocialSharing, private util : UtilityProvider , public events : EventsProvider) {
    this.event_id = this.navParams.get('id');
  }

  getEvents(id) {
    this.obsData = this.events.detailEvents(id);
    this.obsData.subscribe(
      response => {
        console.log("RESULT : ",response);
        this.data = response.data;
      }, error => {
        this.util.showToast(error);
      });
  }

  attend(events_id) {
    this.buttonLoading = true;
    this.obsData = this.events.attendEvents(events_id);
    this.obsData.subscribe(
      response => {
        console.log("RESULT : ",response);
        this.data.status_attend = 1;
        this.buttonLoading = false;
      }, error => {
        this.buttonLoading = false;
        this.util.showToast(error);
      });
  }

  unattend(events_id) {
    this.buttonLoading = true;
    this.obsData = this.events.unattendEvents(events_id);
    this.obsData.subscribe(
      response => {
        console.log("RESULT : ",response);
        this.data.status_attend = 0;
        this.buttonLoading = false;
      }, error => {
        this.buttonLoading = false;
        this.util.showToast(error);
      });
  }

  goToDonateSeedPage(event_id)
  {
    this.navCtrl.push(DonateSeedPage , {event_id : event_id});
  }

  showParticipants(eventId, userType) {
    let limit = 7;
    let offset = 0;
    this.obsData = this.events.getParticipants(eventId, userType, limit, offset);
    this.obsData.subscribe(
      response => {
        console.log("RESULT : ",response);
        this.participantData = response.data;
        this.navCtrl.push(DetailEventParticipantsPage, 
          { 
            eventId: eventId,
            userType: userType, 
            participantData : this.participantData
          });
      }, error => {        
        let usr = (userType == 'friends') ? 'teman' : 'peserta';
        this.util.showToast("Tidak ada " + usr + " yang hadir pada event ini."); 
      });
  }

  shareTo() {
    let msg = "Anda mendapat undangan untuk menanam pohon di event "+this.data.event_name+" Pada tanggal "+this.util.latinDateTime(this.data.event_date)+" dan berlokasi di "+this.data.location+" .Untuk info lebih lanjut, download aplikasinya.";
    let subject = "Undangan acara menanam pohon.";
    let url = "https://play.google.com/store/apps/details?id=com.sinarmasapp.plantrapp&hl=in";
    this.socialSharing.share(msg,subject,null,url).then((success)=> {
      // Sharing via email is possible
      console.log(success);
      //this.util.showToast("Share event berhasil.");
    })
    .catch((error) => {
      console.log("ERROR",error);
      this.util.showToast(error)
      // Sharing via email is not possible
    });
  }

  showMap() {
    this.navCtrl.push(MapsPage , {lat : this.data.lat , lng : this.data.lng});
  }

  getFormatedDate() {
    let concatDateTime = this.data.event_date;
    let check = moment(concatDateTime, 'YYYY-MM-DD');    
    let format = check.format("DD MMM YYYY");
    return format;
  }

  getFormatedTime() {
    let concatDateTime = this.data.event_time;
    let check = moment(concatDateTime, 'HH:mm:ss');    
   
    let format = check.format("hh:mm A ");
    return format;
  }

  ionViewWillEnter() {
    console.log(this.data);
    if(this.data == null)
    {
      this.getEvents(this.event_id); 
    }    
  }
}