import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailEventParticipantsPage } from './detail-eventparticipants';

@NgModule({
  declarations: [
    DetailEventParticipantsPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailEventParticipantsPage),
  ],
})
export class DetailEventParticipantsPageModule {}
