import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';
import { EventsProvider } from '../../providers/events/events';

import { DetailProfilePage } from '../../pages/detail-profile/detail-profile';

import { Observable } from 'rxjs/Observable';
import moment from 'moment';

/**
 * Generated class for the DetailEventParticipantsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-eventparticipants',
  templateUrl: 'detail-eventparticipants.html',
})
export class DetailEventParticipantsPage {
  eventId: number;
  userType: string;
  participantData: any;
  searchKeyword: string;
  obsData : Observable<any>;
  
  loadingMode:string;
  canLoadMore:boolean = true;
  selectedRefresher:any;
  selectedLoader:any;

  DEFAULT_RESULT_LIMIT=7;
  DEFAULT_OFFSET=0;
  limit=7;
  offset=0;
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar : GlobalVarProvider, private util : UtilityProvider, public events : EventsProvider) {
    this.eventId = this.navParams.get('eventId');
    this.userType = this.navParams.get('userType');
    this.participantData = this.navParams.get('participantData');
    this.canLoadMore = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailEventParticipantsPage');
  }

  goToProfilePage(userId)
  {
    this.navCtrl.push(DetailProfilePage, {id : userId});
  }

  submitSearch(isMore : string)
  {
    console.log('submitSearch()');
    console.log(this.searchKeyword);
    this.searchParticipants(this.searchKeyword, isMore);      
  }

  clearSearchBar()
  {
    this.participantData = null;
  }

  onSearchTabChange($event)
  {
    console.log('onSearchTabChange()');
    this.clearSearchBar();
    this.submitSearch("refresh");
  }

  searchParticipants(keyword : string, isMore : string)
  {
    console.log("isMore: " + isMore);
    this.offset += this.limit;

    if(isMore == "refresh") 
    {
      this.limit = this.DEFAULT_RESULT_LIMIT;
      this.offset = this.DEFAULT_OFFSET;
      this.canLoadMore = true;
      this.participantData = [];
    }

    this.obsData = this.events.searchParticipants(this.eventId, keyword, this.userType, this.limit, this.offset);
    this.obsData.subscribe(response => {
      console.log("response participant list", response.data);
      if (this.participantData == null)
      {
        this.participantData = [];
      }      
      this.participantData = this.participantData.concat(this.processParticipantData(response, isMore, this.selectedRefresher));      
      if(this.selectedRefresher) this.selectedRefresher.complete();
    },error => {
      try {
        let errorResponse = JSON.parse(error._body);
        this.util.showToast(errorResponse.message);  
      } catch (exception) {
        this.util.showToast("Peserta tidak ditemukan"); 
      }
     
      if(this.selectedRefresher) this.selectedRefresher.complete();
    });
  }

  processParticipantData(response:any, loadingMode:string, $event : any) 
  {
    let data = [];
    if(response.status == 1)
    {		
      if(loadingMode == 'more')
      {
        console.log('more');
        this.canLoadMore = (response.data.length > 0);
        if(response.data.length > 0)
        {
          data = data.concat(response.data);
        }
        else
        {
          console.log('can\'t load more');
        }        
      }
      else
      {	
        data = response.data;          		
      }              
    }
    else
    {
      this.util.showToast(response.message);
    }

    if($event)
    {
      $event.complete();
    }
    
    return data;
  }

  doRefresh($event:any)
  {
    console.log('doRefresh');
    this.selectedRefresher = $event;
    this.submitSearch("refresh");
  }

  doLoadMore($event:any)
  {
    console.log('doLoadMore');
    this.selectedRefresher = $event;
    this.submitSearch("more");
  }
}