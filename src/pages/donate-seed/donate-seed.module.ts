import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DonateSeedPage } from './donate-seed';

@NgModule({
  declarations: [
    DonateSeedPage,
  ],
  imports: [
    IonicPageModule.forChild(DonateSeedPage),
  ],
})
export class DonateSeedPageModule {}
