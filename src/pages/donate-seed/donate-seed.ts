import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { RefProvider } from '../../providers/ref/ref';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';
import { EventsProvider } from '../../providers/events/events';

/**
 * Generated class for the DonateSeedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-donate-seed',
  templateUrl: 'donate-seed.html',
})
export class DonateSeedPage {
  event_id : any;
  user_id : any;
  number_of_seeds : number = 0;
  obsData : any;
  buttonLoading : boolean = false;
  treeData : any;
  tree_id : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar : GlobalVarProvider, public ref : RefProvider, private util : UtilityProvider , public events : EventsProvider, public alert : AlertController) {
    // console.log('keren');
    this.event_id = this.navParams.get('event_id');
    this.user_id = this.globalVar.myProfile;
    this.getAllTrees();
    console.log(this.event_id);
    console.log(this.user_id);
    // console.log(this.number_of_seeds);
    
    // this.data.profile = this.globalVar.myProfile;
    // this.events.donateSeed(this.event_id, this.user_id, this.number_of_seeds);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DonateSeedPage');
  }

  getAllTrees() {
    this.obsData = this.ref.trees();
    this.obsData.subscribe(response => {
      this.treeData =  response.data;
      console.log(this.treeData);
    }, error => {
       this.util.showToast(error);
    }); 
  }

  donateSeed()
  {
    if (this.number_of_seeds > 0)
    {
      this.buttonLoading = true;
      this.obsData = this.events.donateSeed(this.event_id, this.user_id, this.number_of_seeds, this.tree_id);
      this.obsData.subscribe(response => {
          console.log("RESULT : ",response);
            // this.data.status_attend = 1;
            this.buttonLoading = false;
            let success = response.status;
            let message = response.message;
            if (success)
            {
              console.log('success');
              this.showPopUp();
            }
            else
            {
              this.util.showToast(message);
            }
        },error => {
          this.buttonLoading = false;
          try {
            let errorResponse = JSON.parse(error._body);
            this.util.showToast(errorResponse.message);  
          } catch (exception) {
            this.util.showToast(error);  
          }
        });
    }
    else
    {
      this.util.showToast("Input jumlah bibit terlebih dahulu");
    }
    
  }

  showPopUp() {
    let alert = this.alert.create({
      title: '<center>Terima kasih</center>',
      subTitle: '<br/><center>Terima kasih atas bantuannya.</center><center>Salam lestari</center>',
      buttons: [{
        text: 'DONE',
        handler: () => {
          console.log('Clicked OK');
          this.navCtrl.pop();
        }
      }]
    });
    alert.present();
  }
}
