import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController , ActionSheetController,Platform } from 'ionic-angular';

import { File } from '@ionic-native/file';
import { Transfer ,TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera , CameraOptions } from '@ionic-native/camera';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';
import { UserProvider } from '../../providers/user/user';
import moment from 'moment';
import { Observable } from 'rxjs/Observable';

declare var cordova: any;


@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  email : string;
  username : string;
  fullname : string;
  password : string;

  myProfile : any;
  obsData : Observable<any>;
  namaLengkap : string;
  tglLahir : string;
  imgDiri : string;
  isPickedFoto : boolean = false;
 
  constructor(public navCtrl: NavController, public navParams: NavParams , public globalVar : GlobalVarProvider , public util : UtilityProvider , public alert : AlertController ,private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController ,  public platform: Platform , public user : UserProvider )
  {
    this.username = this.globalVar.myProfile.user.username;
    this.fullname = this.globalVar.myProfile.fullname;
    this.email = this.globalVar.myProfile.user.email;
  }
 
  pickFoto() {
   this.presentActionSheet();
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public getCameraOptions(sourceType) {
    let options : CameraOptions  = {
      quality: 100,
      targetHeight : 800,
      targetWidth : 800,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit : true,
      
    };
   
    return options;
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    let options = this.getCameraOptions(sourceType);
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.util.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.util.createFileName());
      }
    }, (err) => {
      this.util.showToast('Error while selecting image.');
    });
  }

  public copyFileToLocalDir(namePath, currentName, newFileName) {
    
        //-- uncomment when deploying to device --//
    
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.imgDiri = newFileName;
            this.isPickedFoto = true;
          }, 
          error => {
            this.util.showToast('Error while storing file.');
        });
    }

    public pathForImage(img) {
      //console.log(this.myProfile.avatar);
      if (img === null || !this.isPickedFoto) 
      {
        return this.globalVar.myProfile.avatar;
      } 
      else 
      {
          //-- uncomment when deploying to device --//       
          return cordova.file.dataDirectory + img;
      }
    }

    public doEditProfile() {
      
        let paramData = {};
    
        paramData['username'] = this.username;
        paramData['fullname'] = this.fullname;
        paramData['email'] = this.email;
        paramData['password'] = this.password;
        paramData['_method'] = "PUT";

        //@TODO sesuaikan dengan service

        if(this.imgDiri == null  || this.imgDiri == "")
        {
          this.postWithoutImage(paramData);
        }
        else
        {
          this.postWithImage(paramData);
        }
    }

    postWithoutImage(paramData) {
      let loading = this.util.showLoading("Sedang mengupdate data...")
      loading.present();

      this.obsData = this.user.updateProfile(paramData);
      this.obsData.subscribe(response => {
        console.log('RESPONSE: ', response);
        loading.dismissAll();
        this.util.showToast("Update Profil Sukses.");
        
      },error => {
          console.log("error" ,error)
          loading.dismissAll();
          this.util.showToast(error);  
      });
    }

    postWithImage (paramData) {
        // Destination URL
        let url = this.globalVar.urlBase+"/myprofile"; //masih temporary nunggu real service
        // File for Upload
        let targetPath = this.pathForImage(this.imgDiri);
        // File name only
        let filename = this.imgDiri;
  
      var options = {
        fileKey: "avatar",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : paramData // postData here
      };
   
   
    
    options.params['headers'] = { Authorization : "Bearer "+this.globalVar.token};
    console.log(options);    
    const fileTransfer: TransferObject = this.transfer.create();
   
    let loading = this.util.showLoading("Sedang mengupdate data...")
    loading.present();
   
    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
        loading.dismissAll();
        let response = JSON.parse(data.response);
        this.globalVar.myProfile.avatar = response.dataProfiles.avatar;
        console.log("SUKSES Upload Profile",data);
        this.util.showToast("Update Profil Sukses.");
        
    }, error => {
        loading.dismissAll()
        console.log("ERROR",error);   
        this.util.showToast(error);
    });
    }

}
