import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController} from 'ionic-angular';

/**
 * Generated class for the EventPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event-popover',
  templateUrl: 'event-popover.html',
})
export class EventPopoverPage {

  constructor(public viewCtrl: ViewController) {}
  
    close(mode : any) {
      this.viewCtrl.dismiss({viewMode : mode});
    }

}
