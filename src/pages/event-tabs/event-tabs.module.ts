import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventTabsPage } from './event-tabs';
import { NgCalendarModule  } from 'ionic2-calendar';

@NgModule({
  declarations: [
    EventTabsPage,
  ],
  imports: [
    NgCalendarModule,
    IonicPageModule.forChild(EventTabsPage),
  ],
})
export class EventTabsPageModule {}
