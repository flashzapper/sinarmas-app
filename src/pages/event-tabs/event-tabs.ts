import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , PopoverController } from 'ionic-angular';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { EventsProvider } from '../../providers/events/events';
import { UtilityProvider } from '../../providers/utility/utility';
import { Observable } from 'rxjs/Observable';
import moment from 'moment';

import { DetailEventPage } from '../detail-event/detail-event';
import { CreateEventPage } from '../create-event/create-event';
import { EventPopoverPage } from '../event-popover/event-popover';

/**
 * Generated class for the EventTabsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event-tabs',
  templateUrl: 'event-tabs.html',
})
export class EventTabsPage {
  obsData : Observable<any>;
  // data : any;
  myProfileData : any;
  
  // tabs
  selectedRefresher : any;
  selectedEventTab : string;
  dailyEventData : Array<any>;
  monthlyEventData : Array<any>;

  // event calendar
  // http://www.codeexpertz.com/blog/mobile/ionic-2-calendar
  eventSource;
  viewTitle;
  isToday: boolean;
  eventCalendar = {
      mode: 'month',
      currentDate: new Date()
  }; // these are the variable used by the Calendar.
  currentMonth;
  previousMonth;
  shouldUpdateEventData;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar : GlobalVarProvider, public events : EventsProvider, public util : UtilityProvider , public popoverCtrl : PopoverController)
  { 
    this.myProfileData = this.globalVar.myProfile;
    this.dailyEventData = [];
    this.monthlyEventData = [];
    this.shouldUpdateEventData = false;
  }

  // tab event handller
  // first init
  ionViewWillEnter() {
    this.selectedEventTab = "daily";
    
    console.log("ionViewWillEnter");
    console.log(this.selectedEventTab);
    if(this.dailyEventData == null || this.dailyEventData.length == 0)
    {
      this.getEvents(null); 
    }
  }

  onEventTabChange($event)
  {
    console.log('onEventTabChange()');
    this.selectedEventTab = $event.value;
    this.dailyEventData = [];
    this.monthlyEventData = [];

    switch (this.selectedEventTab)
    {
      case ("daily") :
      {
        console.log("daily tab selected");
        this.doDailyEventRefresh(this.selectedRefresher);
        break;
      }
      case ("monthly") :
      {
        console.log("monthly tab selected");
        this.loadEvents();
        break;
      }
    }
  }

  // daily tabs
  getEvents(reforInf:any) {
    this.obsData = this.events.events();
      this.obsData.subscribe(response => {
        console.log("response event",response.data);
        this.dailyEventData = response.data;
        if(reforInf) reforInf.complete();
      },error => {
        this.util.showToast(error);
        if(reforInf) reforInf.complete();
      });     
  }

  doDailyEventRefresh(refresher) 
  {
    this.selectedRefresher = refresher;
    this.getEvents(refresher);
  }

  // monthly tab
  // date format: [Y-M-D hh:mm:ss]
  getMonthlyEventsByDate(startDate, endDate) //, reforInf:any)
  {
    let tempEvent = [];
    this.obsData = this.events.events();
    this.obsData.subscribe(response => {
      console.log("response event",response.data);
      this.monthlyEventData = response.data;

      this.monthlyEventData.forEach(eventData => {
        let eventList = eventData.events;
        eventList.forEach(event => {
          let eventStartDateTime = this.stringToDate(event.event_date + " " + event.event_time);
          let eventEndDateTime = this.stringToDate(event.event_date + " 23:59:59");
          
          // console.log(eventTimeString);
          // console.log(eventStartDateTime);
          // console.log(eventEndDateTime);
          tempEvent.push({
              id: event.id,
              title: event.event_name,
              startTime: eventStartDateTime,
              endTime: eventEndDateTime,
              allDay: false
          });        
        });
      });

      this.eventSource = tempEvent;
      
      // if(reforInf) reforInf.complete();
    },error => {
      this.util.showToast(error);
      // if(reforInf) reforInf.complete();
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(EventPopoverPage);
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss((data) => {
      let mode = data.viewMode;
      this.changeMode(mode);
    })
  }


  // eventCalendar
  loadEvents() {
    let dateRange = this.getCurrentStartAndEndDate();
    let startDate = dateRange.start_date;
    let endDate = dateRange.end_date;
    this.getMonthlyEventsByDate(startDate, endDate);
   // this.eventSource = this.getMonthlyEventsByDate(startDate, endDate);
    // this.eventSource = this.createRandomEvents();
    
    console.log("loadEvents()");
    console.log(this.eventSource);
  }
  onViewTitleChanged(title) {
      this.previousMonth = this.currentMonth;
      this.viewTitle = title;
      this.currentMonth = title;
      console.log(this.eventCalendar);
      // console.log("onViewTitleChanged() " + this.previousMonth + " " + this.currentMonth);
  }
  onEventSelected(event) {
    console.log(event.id);
    this.goToDetailEventById(event.id);
      // console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
  }
  changeMode(mode) {
      this.eventCalendar.mode = mode;
  }
  today() {
      this.eventCalendar.currentDate = new Date();
  }
  onTimeSelected(ev) 
  {
    console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
    (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
    // if (this.shouldUpdateEventData)
    // {
    //   console.log("bulannya beda");
    //   let startDate = this.getFirstDateInThisMonth(ev.selectedTime);
    //   let endDate = this.getFirstDateInNextMonth(ev.selectedTime);
    //   this.eventSource = this.getMonthlyEventsByDate(startDate, endDate);
    //   this.shouldUpdateEventData = false;
    // }

    // if (this.currentMonth != this.previousMonth)
    // {
    //   this.shouldUpdateEventData = true;
    // }

      // console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
      //     (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
  }
  onCurrentDateChanged(event:Date) {


      var today = new Date();
      today.setHours(0, 0, 0, 0);
      event.setHours(0, 0, 0, 0);
      this.isToday = today.getTime() === event.getTime();

      // update the event Data      
      // let startDate = this.getFirstDateInThisMonth(event);
      // let endDate = this.getFirstDateInNextMonth(event); 
      // this.eventSource = this.getMonthlyEventsByDate(startDate, endDate);     
      // console.log("onCurrentDateChanged");
      // console.log(event);
      // console.log([startDate, endDate]);
      // console.log(this.eventSource);
  }

  getCurrentStartAndEndDate()
  {
    let currentDate = new Date();
    // let startDateMoment = moment(new Date(currentDate.getFullYear(), currentDate.getMonth(), 1));
    let startDate = this.getFirstDateInTheMonth(currentDate); //startDateMoment.format("YYYY-MM-DD");
    let endDate = this.getFirstDateInNextMonth(currentDate); //startDateMoment.add(1, "months").format("YYYY-MM-DD");

    let result = {
      start_date: startDate,
      end_date: endDate
    }

    console.log("getCurrentStartAndEndDate()");
    console.log(result);

    return result;
  }

  getFirstDateInTheMonth(date)
  {
    return moment(new Date(date.getFullYear(), date.getMonth(), 1)).format("Y-M-D hh:mm:ss"); //.format("YYYY-MM-DD");
  }

  getFirstDateInNextMonth(date)
  {
    return moment(new Date(date.getFullYear(), date.getMonth(), 1)).add(1, "months").format("Y-M-D hh:mm:ss"); //.format("YYYY-MM-DD");    
  }

  // https://stackoverflow.com/questions/40238061/how-to-change-format-of-a-date-from-dd-mm-yyyy-to-mm-dd-yyyy-where-date-is-alrea
  stringToDate(stringDate) // format: YYYY-MM-DD
  {    
    return new Date(stringDate);
  }

  dateToString(date) // format: YYYY-MM-DD
  {    
    return moment(date).format("YYYY-MM-DD");
  }

  timeToString(time)
  {
    return moment(time).format("HH:MM:SS");
  }

  // createRandomEvents() {
  //     var events = [];
  //     for (var i = 0; i < 50; i += 1) {
  //         var date = new Date();
  //         var eventType = Math.floor(Math.random() * 2);
  //         var startDay = Math.floor(Math.random() * 90) - 45;
  //         var endDay = Math.floor(Math.random() * 2) + startDay;
  //         var startTime;
  //         var endTime;
  //         console.log("interasi ke-" + i + " startDay: " + startDay + " endDay: " + endDay + " startTime: " + startTime + " endTime: " + endTime);
  //         if (eventType === 0) {
  //             startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
  //             if (endDay === startDay) {
  //                 endDay += 1;
  //             }
  //             endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
  //             events.push({
  //                 title: 'All Day - ' + i,
  //                 startTime: startTime,
  //                 endTime: endTime,
  //                 allDay: true
  //             });
  //         } else {
  //             var startMinute = Math.floor(Math.random() * 24 * 60);
  //             var endMinute = Math.floor(Math.random() * 180) + startMinute;
  //             startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
  //             endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
  //             events.push({
  //                 title: 'Event - ' + i,
  //                 startTime: startTime,
  //                 endTime: endTime,
  //                 allDay: false
  //             });
  //         }
  //     }
  //     return events;
  // }
  onRangeChanged(ev) {
      console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
  }
  markDisabled = (date:Date) => {
      var current = new Date();
      current.setHours(0, 0, 0);
      return date < current;
  };

  // other methods
  getYearName(val) {
    let check = moment(val, 'YYYY-MM-DD');    
    let month = check.format('YYYY');
    return month;
  }
  
  getMonthName(val) {
    let check = moment(val, 'YYYY-MM-DD');    
    let month = check.format('MMM');
    return month;
  }

  getDayName(val) {
    let check = moment(val, 'YYYY-MM-DD');    
    let day = check.format('D');
    return day;
  }

  getFormatedDate(val) {
    let concatDateTime = val;
    let check = moment(concatDateTime, 'YYYY-MM-DD');    
    let format = check.format("DD MMM YYYY");
    return format;
  }

  getFormatedTime(val) {
    let concatDateTime = val;
    let check = moment(concatDateTime, 'HH:mm:ss');    
   
    let format = check.format("hh:mm A ");
    return format;
  }

  goToDetailEvent(data) {
    this.goToDetailEventById(data.id);
  }
  
  goToDetailEventById(eventId) {
    this.navCtrl.push(DetailEventPage , {id : eventId});
  }

  goToAddEvent() {
    this.navCtrl.push(CreateEventPage);
  }

  goToEventPage()
  {
     this.navCtrl.push(EventTabsPage);
  }
}
