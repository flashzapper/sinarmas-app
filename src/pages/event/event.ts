import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { EventsProvider } from '../../providers/events/events';
import { UtilityProvider } from '../../providers/utility/utility';
import { Observable } from 'rxjs/Observable';
import moment from 'moment';

import { SearchTabsPage } from '../search-tabs/search-tabs';
import { DetailEventPage } from '../detail-event/detail-event';
import { CreateEventPage } from '../create-event/create-event';

/**
 * Generated class for the EventPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {
  obsData : Observable<any>;
  data : any;
  myProfileData : any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar : GlobalVarProvider, public events : EventsProvider, public util : UtilityProvider)
  { 
    this.myProfileData = this.globalVar.myProfile;
  }


  getEvents(reforInf:any) {
    this.obsData = this.events.events();
      this.obsData.subscribe(response => {
        console.log("response event",response.data);
        this.data = response.data;
        if(reforInf) reforInf.complete();
      },error => {
        this.util.showToast(error);
        if(reforInf) reforInf.complete();
      });     
  }

  doRefresh(refresher) {
    this.getEvents(refresher);
  }

  getMonthName(val) {
    let check = moment(val, 'YYYY-MM-DD');    
    let month = check.format('MMM');
    return month;
  }

  getDayName(val) {
    let check = moment(val, 'YYYY-MM-DD');    
    let day = check.format('D');
    return day;
  }

  getFormatedDate(val) {
    let concatDateTime = val;
    let check = moment(concatDateTime, 'YYYY-MM-DD');    
    let format = check.format("DD MMM YYYY");
    return format;
  }

  getFormatedTime(val) {
    let concatDateTime = val;
    let check = moment(concatDateTime, 'HH:mm:ss');    
   
    let format = check.format("hh:mm A ");
    return format;
  }

  goToDetailEvent(data) {
    this.navCtrl.push(DetailEventPage , {id : data.id});
  }

  goToAddEvent() {
    this.navCtrl.push(CreateEventPage);
  }


  ionViewWillEnter() {
    console.log(this.data);
    if(this.data == null)
    {
      this.getEvents(null); 
    }
  }

  goToSearchPage()
  {
     this.navCtrl.push(SearchTabsPage);
  }
}