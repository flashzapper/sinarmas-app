import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { RegisterPage } from '../register/register';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the FirstPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-first',
  templateUrl: 'first.html',
})
export class FirstPage {

  constructor(public navCtrl: NavController, public navParams: NavParams , public auth : AuthProvider , public globalVar : GlobalVarProvider) {
  }

  goLogin() {
      this.navCtrl.push(LoginPage);
  }

  goRegister() {
       this.navCtrl.push(RegisterPage);
  }

  guest() {
    this.globalVar.loginState = false;
    this.navCtrl.push(TabsPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstPage');
  }

}
