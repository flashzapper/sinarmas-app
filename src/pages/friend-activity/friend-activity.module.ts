import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FriendActivityPage } from './friend-activity';

@NgModule({
  declarations: [
    FriendActivityPage,
  ],
  imports: [
    IonicPageModule.forChild(FriendActivityPage),
  ],
})
export class FriendActivityPageModule {}
