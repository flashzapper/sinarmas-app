import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InboxPopupPage } from './inbox-popup';

@NgModule({
  declarations: [
    InboxPopupPage,
  ],
  imports: [
    IonicPageModule.forChild(InboxPopupPage),
  ],
})
export class InboxPopupPageModule {}
