import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';
import { InboxProvider } from '../../providers/inbox/inbox';

import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the InboxPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
})
export class InboxPage {
  REQUEST_TO_DONATE = 0;
  APPROVE_DONATION = 1;
  REJECT_DONATION = 2;

  userId: number;
  inboxData: any;
  searchKeyword: string;
  obsData : Observable<any>;
  
  loadingMode:string;
  canLoadMore:boolean = true;
  refresher:any;
  loader:any;

  DEFAULT_RESULT_LIMIT=7;
  DEFAULT_OFFSET=0;
  limit=7;
  offset=0;
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar : GlobalVarProvider, private util : UtilityProvider, public inboxProvider : InboxProvider, public alert : AlertController) {
    this.inboxData = this.navParams.get('inboxData');
    this.userId = this.navParams.get('userId');
    this.canLoadMore = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InboxPage');
  }

  submitSearch(isMore : string)
  {
    console.log('submitSearch()');
    console.log(this.searchKeyword);
    this.searchSeedDonationInboxMessages(this.searchKeyword, isMore);      
  }

  clearSearchBar()
  {
    this.inboxData = null;
  }

  onSearchTabChange($event)
  {
    console.log('onSearchTabChange()');
    this.clearSearchBar();
    this.submitSearch("refresh");
  }

  searchSeedDonationInboxMessages(keyword : string, isMore : string)
  {
    console.log("isMore: " + isMore);
    this.offset += this.limit;

    if(isMore == "refresh") 
    {
      this.limit = this.DEFAULT_RESULT_LIMIT;
      this.offset = this.DEFAULT_OFFSET;
      this.canLoadMore = true;
      this.inboxData = [];
    }

    this.obsData = this.inboxProvider.searchSeedDonationInboxMessages(keyword, this.limit, this.offset);
    this.obsData.subscribe(response => {
      console.log("response participant list", response.data);
      if (this.inboxData == null)
      {
        this.inboxData = [];
      }      
      this.inboxData = this.inboxData.concat(this.processInboxData(response, isMore, this.refresher));      
      if(this.refresher) this.refresher.complete();
    },error => {
      try {
        let errorResponse = JSON.parse(error._body);
        this.util.showToast(errorResponse.message);  
      } catch (exception) {
        this.util.showToast("Pesan tidak ditemukan"); 
      }
     
      if(this.refresher) this.refresher.complete();
    });
  }

  processInboxData(response:any, loadingMode:string, $event : any) 
  {
    let data = [];
    if(response.status == 1)
    {		
      if(loadingMode == 'more')
      {
        console.log('more');
        this.canLoadMore = (response.data.length > 0);
        if(response.data.length > 0)
        {
          data = data.concat(response.data);
        }
        else
        {
          console.log('can\'t load more');
        }        
      }
      else
      {	
        data = response.data;          		
      }              
    }
    else
    {
      this.util.showToast(response.message);
    }

    if($event)
    {
      $event.complete();
    }
    
    return data;
  }

  doRefresh($event:any)
  {
    console.log('doRefresh');
    this.refresher = $event;
    this.submitSearch("refresh");
  }

  doLoadMore($event:any)
  {
    console.log('doLoadMore');
    this.refresher = $event;
    this.submitSearch("more");
  }

  // donation
  confirmSeedDonation(inbox)
  {
    let seedEventApprovalId = inbox.id;
    if (inbox.approval_status == this.REQUEST_TO_DONATE)
    {
      this.showConfirmSeedDonationPopUp(inbox, seedEventApprovalId);
    }
    else if (inbox.approval_status == this.APPROVE_DONATION)
    {
      this.showDonorDetailsPopUp(inbox);
    }

    // read the message
    if ((inbox.id_user_event_owner == this.userId && !inbox.has_been_read_by_event_owner) 
      || (inbox.id_user_donor == this.userId && !inbox.has_been_read_by_donor)) {
        this.readSeedDonationInboxMessage(inbox, seedEventApprovalId);
    }
  }

  showConfirmSeedDonationPopUp(inbox, seedEventApprovalId)
  {
    let donation_time = this.util.formattedDate(inbox.donation_time);

    let alert = this.alert.create({
      title: '<center>Konfirmasi</center>',
      cssClass: 'popup-head popup-title',
      subTitle: '<br/><div>' + inbox.donor.name + ' mendonasikan ' + inbox.donate_seed + ' buah bibit ' + inbox.tree.tree_name + ' kepada Anda!</div><div>' + donation_time + '</div>',
      buttons: [
        {
          text: 'Terima',
          cssClass: 'popup-buttons',
          handler: () => {
            console.log('approve donation');
            this.approveDonation(seedEventApprovalId);
          }
        },
        {
          text: 'Tolak',
          cssClass: 'popup-buttons',
          handler: () => {
            console.log('reject donation');
            this.rejectDonation(seedEventApprovalId);
          }
        }
      ]
    });
    alert.present();
  }
  
  readSeedDonationInboxMessage(inbox, seedEventApprovalId)
  {
    this.obsData = this.inboxProvider.readSeedDonationInboxMessage(seedEventApprovalId);
    this.obsData.subscribe(response => {
      console.log("response", response);
      console.log("Sukses membaca pesan");
      inbox = response.data;
      // this.util.showToast("Sukses membaca pesan"); 
    }, error => {
      try {
        let errorResponse = JSON.parse(error._body);
        console.log(errorResponse.message);  
        // this.util.showToast("Gagal membaca pesan");  
      } catch (exception) {
        // this.util.showToast("Gagal membaca pesan"); 
      }      
    });
  }

  approveDonation(seedEventApprovalId)
  {
    this.obsData = this.inboxProvider.approveDonation(seedEventApprovalId);
    this.obsData.subscribe(response => {
      console.log("response", response);
      let success = response.status;
      let message = response.message;
      if(success)
      {
        this.util.showToast("Sukses menerima donasi"); 
        this.navCtrl.pop();
      }
      else
      {
        this.util.showToast(message); 
      }
    }, error => {
      try {
        let errorResponse = JSON.parse(error._body);
        console.log(errorResponse.message);  
        this.util.showToast("Gagal menerima donasi");  
      } catch (exception) {
        this.util.showToast("Gagal menerima donasi"); 
      }      
    });
  }

  rejectDonation(seedEventApprovalId)
  {
    this.obsData = this.inboxProvider.rejectDonation(seedEventApprovalId);
    this.obsData.subscribe(response => {
      console.log("response", response);
      let success = response.status;
      let message = response.message;
      if(success)
      {
        this.util.showToast("Sukses menolak donasi"); 
        this.navCtrl.pop();
      }
      else
      {
        this.util.showToast(message); 
      }
    }, error => {
      try {
        let errorResponse = JSON.parse(error._body);
        console.log(errorResponse.message);  
        this.util.showToast("Gagal menolak donasi");  
      } catch (exception) {
        this.util.showToast("Gagal menolak donasi"); 
      }      
    });
  }

  // http://pointdeveloper.com/how-to-make-a-phone-call-with-ionic-apps/
  showDonorDetailsPopUp(inbox) 
  {    
    let avatar = '<img class="avatar" src="' + this.util.checkAvatar(inbox.donor.profile.avatar) + '" class="profile-picture">';
    let name = inbox.donor.profile.fullname;
    let email = inbox.donor.email;
    let address = (typeof inbox.donor.address !== "undefined") ? inbox.donor.address : 'N/A';
    let tlp = (inbox.donor.profile.phone_number == null) ? 'N/A' : '<a href="tel:' + inbox.donor.profile.phone_number + '" class="button button-positive">'+ inbox.donor.profile.phone_number +'</a>';
    
    let alert = this.alert.create({
      title: '<center>Konfirmasi</center>',
      cssClass: 'popup-head popup-title',
      subTitle: '<br/><center>'+avatar+'</center><br/><div>Berikut adalah profil lengkap</div><div>Donatur Anda:</div><br/><div>Nama: '+name+'</div><br/><div>Email: '+email+'</div><br/><div>Address: '+address+'</div><br/><div>Telepon: '+tlp+'</div>',
      buttons: [{
        text: 'OK',
        handler: () => {
          console.log('Clicked OK');
        }
      }]
    });
    alert.present();
  }

  readMessage(inbox)
  {
    let seedEventApprovalId = inbox.id;
    let donation_time = this.util.formattedDate(inbox.donation_time);
    let approval_status = inbox.approval_status;
    let message = "";

    switch (approval_status) {
      case this.APPROVE_DONATION:
      {
        message = "menerima";
        break;
      }    
      case this.REJECT_DONATION:
      {
        message = "menolak";
        break;
      }   
      default:
      {
        message = "sedang mengonfirmasi";
        break;
      }
    }

    let alert = this.alert.create({
      title: '<center>Konfirmasi</center>',
      cssClass: 'popup-head popup-title',
      subTitle: '<br/><div>' + inbox.recipient.fullname + ' ' + message + ' donasi bibit dari Anda</div><div>' + donation_time + '</div>'      
    });
    alert.present();

    // read the message
    if ((inbox.id_user_event_owner == this.userId && !inbox.has_been_read_by_event_owner) 
      || (inbox.id_user_donor == this.userId && !inbox.has_been_read_by_donor)) {
        this.readSeedDonationInboxMessage(inbox, seedEventApprovalId);
    }
  }
}