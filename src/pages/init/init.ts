import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { IntroPage } from '../intro/intro';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UserProvider } from '../../providers/user/user';

import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the InitPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-init',
  templateUrl: 'init.html',
})
export class InitPage {
  tabBarElement :any;
  obsData : Observable<any>;


  constructor(public navCtrl: NavController, public navParams: NavParams , public storage : Storage , public globalVar : GlobalVarProvider , public user : UserProvider) {
    
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    console.log("TABBAR",this.tabBarElement);
    
    if(this.tabBarElement)
    {
      this.tabBarElement.style.display = "none";
    }
    
    
    storage.get('isRegistered').then((val) => {   
      this.globalVar.loginState = true;
      console.log(this.globalVar.loginState);
      if(val == 1)
        {
          storage.get('token').then((token) => {
              this.globalVar.token = token;
              this.obsData = this.user.getMyProfile();
              this.obsData.subscribe(response => {
                console.log(response.data);
                this.globalVar.myProfile = response.data;  
                navCtrl.setRoot(TabsPage, {} ,{
                  animate: true,
                  direction: 'forward' 
                });      
            },error => {
              console.log("ERROR",error); 
            });
   
          });
        }
        else
        {
          setTimeout(() => {
             navCtrl.setRoot(IntroPage, {} ,{
              animate: true,
              direction: 'forward' 
            }); 
        }, 1000);
        } 
    });
  }

  ionViewDidLoad() {
  }

}
