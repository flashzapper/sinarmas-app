import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirstPage } from '../first/first';

/**
 * Generated class for the IntroPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})

// export interface Slide {
//   title: string;
//   description: string;
//   image: string;
// }
export class IntroPage {

//  slides: Slide[];
  public showSkip = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  goFirstPage() {
    this.navCtrl.setRoot(FirstPage, {}, {
      animate: true,
      direction: 'forward'
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
  }

}
