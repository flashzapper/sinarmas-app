import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { ActivityProvider } from '../../providers/activity/activity';
import { UserProvider } from '../../providers/user/user';
import { UtilityProvider } from '../../providers/utility/utility';
import { Observable } from 'rxjs/Observable';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
 } from '@ionic-native/google-maps';

 import { DetailContentPage } from '../detail-content/detail-content';

@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  selectedTab : string;
  obsData : Observable<any>;
  obsData2 : Observable<any>;
  data : any;
  limit : number = 10000;
  offset : number = 0;
  canLoadMore : boolean = true;
  loginState : boolean;

  map: GoogleMap;
  mapElement: HTMLElement;

  constructor(public navCtrl: NavController, public navParams: NavParams , public globalVar : GlobalVarProvider , public activity : ActivityProvider , public util : UtilityProvider ,private googleMaps: GoogleMaps  , public user : UserProvider) {
    this.selectedTab = "all";
    this.getActivity('refresh',null);

    this.loginState = this.globalVar.loginState;
  }
  ionViewWillEnter() {
    
  }

  ionViewWillLeave () {
   
  }

  onTabChange($event)
  {
    this.selectedTab = $event.value;
    switch (this.selectedTab)
    {
      case ("me") :
      {
        console.log("my trees");
        this.getMyProfile(null);
        break;
      }
      case ("all") :
      {
        console.log("all trees");
        this.getActivity("refresh",null);
        break;
      }
    }
  }

  getMyProfile(reforInf:any) {
    this.obsData2 = this.user.getMyProfile();
      this.obsData2.subscribe(response => {
      this.data = response.data.posting;
      console.log(this.data);
      this.showMap();

        if(reforInf) reforInf.complete();
      },error => {
        this.util.showToast(error);
        if(reforInf) reforInf.complete();
      });     
  }


  getActivity(isMore : string , refOrInf : any) {
    this.limit = this.limit;
    this.offset = this.offset + this.limit;

    if(isMore == "refresh") 
    {
      this.limit = 10000;
      this.offset = 0;
      this.canLoadMore = true;
    }


      this.obsData = this.activity.getActivity(this.limit,this.offset,"ALL");
      this.obsData.subscribe(response => {
          this.processData(response,isMore,refOrInf);
          this.showMap();
      },error => {
        if(refOrInf)
        {
          refOrInf.complete();
        }			
        this.util.showToast(error.message);
      }); 
    
  }

  processData(response:any , isMore:string, refOrInf : any) {
    if(response.status == 1)
      {		
        if(isMore == 'more')
        {
          console.log('more');
          if(response.data.length <= 0)
          {
            this.canLoadMore = false;
            console.log('cant load more');
            
          }
          else
          {
            this.canLoadMore = true;
            this.data = this.data.concat(response.data);
            if(refOrInf)
            {
              refOrInf.complete();
            }			
          }
          
        }
        else
        {	
          console.log(response.data);
          this.data = response.data;
          if(refOrInf)
            {
              refOrInf.complete();
            }			
        }              
      }
      else
      {
        this.util.showToast(response.data.message);  
        if(refOrInf)
          {
            refOrInf.complete();
          }		
                    
      }
  }

  showMap() {
    let baseLat = "-0.789275";
    let baseLng = "113.921327";
    this.mapElement = document.getElementById('map');
    
       let mapOptions: GoogleMapOptions = {
         camera: {
           target: {
             lat: baseLat,
             lng: baseLng
           },
           zoom: 4,
           tilt: 30
         }
       };
       
       if(this.map == null)
       this.map = this.googleMaps.create(this.mapElement, mapOptions);
       else 
       this.populateMarker();
       
       
       // Wait the MAP_READY before using any methods.
       this.map.on(GoogleMapsEvent.MAP_READY)
         .subscribe(() => {
           console.log('Map is ready!');
           this.populateMarker();
         }); 
  }
  
  populateMarker() {
    this.map.clear();
    for(let i = 0; i < this.data.length ; i++)
    {
      console.log("attempt marking :", "ke-"+i);
      let currentCaption;
      if(!this.data[i].caption)
      {
        currentCaption = "";
      }
      else
      {
        currentCaption = this.data[i].caption;
      }

       this.map.addMarker({  
        title: currentCaption,
        snippet : "Posted by : "+this.data[i].created_by,
        disableAutoPan : false,
        icon: 'red',
        animation: 'DROP',
        position: {
          lat: this.data[i].lat,
          lng: this.data[i].lng
        }
      })
      .then(marker => {
        console.log("dropped marker :", "ke-"+i);
        marker.on(GoogleMapsEvent.MARKER_CLICK)
          .subscribe(() => {
            console.log('clicked ! ');
            let id = this.data[i].id;   
            this.navCtrl.push(DetailContentPage,{id : id , myProfile : true});
          });
      });
    }
    
  }

  

}
