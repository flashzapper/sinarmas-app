import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { TabsPage } from '../tabs/tabs';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public username : string;
  public password : string;
  loginData : Observable<any>;
  obsData : Observable<any>;

  constructor(private app :App , public navCtrl: NavController, public navParams: NavParams , public toastCtrl: ToastController , public globalVar : GlobalVarProvider , public auth : AuthProvider , public user: UserProvider) {
  }

  doLogin() {
    let postData = {};
    postData['login'] = this.username;
    postData['password'] = this.password;

    console.log("postData" , postData);

    this.loginData = this.auth.login(postData);
    this.loginData.subscribe(data => {
      console.log('my data: ', data);
      if(data.status == 1)
        {
          this.globalVar.setToken(data.token);
          this.globalVar.setRegistered(1);
          this.globalVar.loginState = true;
          this.loadMyProfile();
        }
      else
        {
          this.showToast(data.message);         
        }
     
    },error => {
      console.log("error" ,error)
      this.showToast(error);  
    });

  }

  loadMyProfile() {
    this.loginData = this.user.getMyProfile();
    this.loginData.subscribe(response => {
      console.log('my data: ', response);
      this.globalVar.myProfile = response.data;  
      this.navCtrl.setRoot(TabsPage, {}, {
        animate: true,
        direction: 'forward'
      });
    },error => {
        console.log("error" ,error)
        this.showToast(error);  
    });
  }

  guest() {
    this.globalVar.loginState = false;
    this.navCtrl.push(TabsPage);
  }

  showToast(text:string) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      showCloseButton : true
   });
   toast.present();


    // toast.onDidDismiss(() => {
    //   this.navCtrl.setRoot(TabsPage);
    // });

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  back(){
    this.navCtrl.pop();
  }

}
