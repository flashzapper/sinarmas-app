import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,LatLng
 } from '@ionic-native/google-maps';

import { Component , ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams ,Platform , ViewController} from 'ionic-angular';
import { GlobalVarProvider } from '../../providers/global-var/global-var';

import { UtilityProvider } from '../../providers/utility/utility';
import { GoogleGeocodeProvider } from '../../providers/google-geocode/google-geocode';

@IonicPage()
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})
export class MapsPage {

  @ViewChild('map') mapElement: ElementRef;

  map: GoogleMap;
  //mapElement: HTMLElement;
  lat : any;
  lng : any;

  constructor(private googleMaps: GoogleMaps , public navCtrl : NavController , public navParams : NavParams , public globalVar : GlobalVarProvider , public util : UtilityProvider , public view : ViewController ,  private plt: Platform , private mapServiceProvider : GoogleGeocodeProvider) {
     console.log(this.navParams);
  }

  goBack() {
    this.view.dismiss();
  }
   
  ionViewWillEnter() {
    // if(!this.globalVar.loginState)
    // {
    //   setTimeout(() => {
    //     this.util.alertGuest("m",null);
    //    }, 2000);
    // }
     
    console.log('ionViewDidEnter Home');
    this.loadMap();
      
     }

    ionViewWillLeave () {
      this.mapServiceProvider.setDiv(null);
    }

  loadMap() {
    try
    {
      this.lat = this.navParams.get('lat');
      this.lng = this.navParams.get('lng');
      
      let lat = this.lat;
      let lng = this.lng;

      if(lat != null || lng != null)
      {
        this.mapServiceProvider.setDiv(this.mapElement.nativeElement).then(() => {
          //add markers, polyline, etc.
          this.mapServiceProvider.map.addMarker({
            position: {
              lat: this.lat,
              lng: this.lng
            }
          });

          this.mapServiceProvider.map.animateCameraZoomOut();
     
        });
       }
    }
    catch (ex)
    {
      console.log("CATCH :",ex);
    }
  }
}
