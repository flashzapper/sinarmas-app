import { Component , ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UserProvider } from '../../providers/user/user';
import { UtilityProvider } from '../../providers/utility/utility';
import { InboxProvider } from '../../providers/inbox/inbox';
import { Observable } from 'rxjs/Observable';

import { InboxPage } from '../inbox/inbox';
import { SearchTabsPage } from '../search-tabs/search-tabs';
import { DetailContentPage } from '../detail-content/detail-content';
import { SettingPage } from '../setting/setting';


import { GoogleGeocodeProvider } from '../../providers/google-geocode/google-geocode';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,LatLng
 } from '@ionic-native/google-maps';


/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  @ViewChild('map') mapElement: ElementRef;
  
  map: GoogleMap;
  lat : any;
  lng : any;

  obsData : Observable<any>;
  data : any;
  rows : any;
  selectedTab : any;
  userId : number;

  constructor(public navCtrl: NavController, public navParams: NavParams , public globalVar : GlobalVarProvider , public user : UserProvider , public util : UtilityProvider , private googleMaps: GoogleMaps , private mapServiceProvider : GoogleGeocodeProvider, public inboxProvider : InboxProvider) {
     this.selectedTab = 'grid';
  }

  getMyProfile(reforInf:any) {
    this.obsData = this.user.getMyProfile();
      this.obsData.subscribe(data => {
        console.log(data);
        this.userId =  data.data.user_id;
        this.globalVar.myProfile = data.data;
        this.globalVar.totalUnreadInboxMessages = data.total_messages;
        this.data = data;
        this.rows = Array.from(Array(Math.ceil(this.data.data.posting.length / 3)).keys());

        this.loadMap();

        if(reforInf) reforInf.complete();
      },error => {
        this.util.showToast(error);
        if(reforInf) reforInf.complete();
      });     
  }

  doRefresh(refresher) {
     this.getMyProfile(refresher); 
  }

  goDetail(id) {
    this.navCtrl.push(DetailContentPage,{id : id , myProfile : true});
  }

  onSearchTabChange($event)
  {
    console.log(this.selectedTab);
    this.selectedTab = $event.value;
    if(this.selectedTab == 'maps')
    {
      setTimeout(() => {
        this.loadMap();
       }, 1000);
    }
    else
    {
      this.mapServiceProvider.setDiv(null);
    }
  }

  loadMap() {
    try
    {
      console.log("Init Load Map.");
        this.mapServiceProvider.setDiv(this.mapElement.nativeElement).then(() => {
          //add markers, polyline, etc.
          this.populateMarker();
          this.mapServiceProvider.map.animateCameraZoomOut();
     
        });
    }
    catch (ex)
    {
      console.log("CATCH :",ex);
    }
  }
  populateMarker() {

    for(let i = 0; i < this.data.data.posting.length ; i++)
    {
      console.log("attempt marking :", "ke-"+i);
      let currentCaption;
      if(!this.data.data.posting[i].caption)
      {
        currentCaption = "";
      }
      else
      {
        currentCaption = this.data.data.posting[i].caption;
      }

       this.mapServiceProvider.map.addMarker({  
        title: currentCaption,
        snippet : "Posted by : "+this.data.data.posting[i].created_by,
        disableAutoPan : false,
        icon: 'red',
        animation: 'DROP',
        position: {
          lat: this.data.data.posting[i].lat,
          lng: this.data.data.posting[i].lng
        }
      })
      .then(marker => {
        console.log("dropped marker :", "ke-"+i);
        marker.on(GoogleMapsEvent.MARKER_CLICK)
          .subscribe(() => {
            console.log('clicked ! ');
            let id = this.data.data.posting[i].id;   
           // this.navCtrl.push(DetailContentPage,{id : id});
          });
      });
    }
    
  }

  goToSettings(){
    this.navCtrl.push(SettingPage,{});
  }

  ionViewDidEnter() {
    // ionViewWillEnter() {
      console.log(this.data);
      if(this.data == null)
      {
        this.getMyProfile(null);
      }
  
  }

  ionViewWillLeave () {
     this.selectedTab == 'grid';
     this.mapServiceProvider.setDiv(null);
  }

  back(){
    this.navCtrl.pop();
  }

  goToSearchPage()
  {
     this.navCtrl.push(SearchTabsPage);
  }

  showInboxMessages() 
  {
    let limit = 7;
    let offset = 0;
    this.obsData = this.inboxProvider.getSeedDonationInboxMessages(limit, offset);
    this.obsData.subscribe(
      response => {
        console.log("RESULT : ",response);
        let inboxData = response.data;
        this.navCtrl.push(InboxPage, 
          {  
            inboxData : inboxData,
            userId : this.userId
          });
      }, error => {        
        this.util.showToast("Tidak ada pesan."); 
      });
  }
}