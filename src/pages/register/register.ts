import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilityProvider } from '../../providers/utility/utility';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register', 
  templateUrl: 'register.html',
})
export class RegisterPage {

  public email : string;
  public username : string;
  public password : string;
  public fullname : string;
  loginData : Observable<any>;
  registerData : Observable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams ,public globalVar : GlobalVarProvider , public auth : AuthProvider , public util : UtilityProvider , public user: UserProvider) {
  }

  
  

  doRegister() {
    let postData = {};
    postData['email'] = this.email;
    postData['username'] = this.username;
    postData['password'] = this.password;
    postData['fullname'] = this.fullname;

    console.log("postData" , postData);

    this.registerData = this.auth.register(postData);
    this.registerData.subscribe(data => {
      console.log('my data: ', data);
      if(data.status == 1)
        {
          this.globalVar.setToken(data.token);
          this.globalVar.setRegistered(1);
          this.globalVar.loginState = true;
          this.loadMyProfile();

         
        }
      else
        {
          this.util.showToast(data.message);         
        }
     
    },error => {
      console.log("error" ,error)
      this.util.showToast(error.body);  
    });
  }

  loadMyProfile() {
    this.loginData = this.user.getMyProfile();
    this.loginData.subscribe(response => {
      console.log('my data: ', response);
      this.globalVar.myProfile = response.data;  
      this.navCtrl.setRoot(TabsPage, {}, {
        animate: true,
        direction: 'forward'
      });
    },error => {
        console.log("error" ,error)
        this.util.showToast(error);  
    });
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  back(){
    this.navCtrl.pop();
  }


}
