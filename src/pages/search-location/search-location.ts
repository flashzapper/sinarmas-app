import { Component } from '@angular/core';
import { IonicPage, ViewController  } from 'ionic-angular';
import { GoogleGeocodeProvider } from '../../providers/google-geocode/google-geocode';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the LocationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-location',
  templateUrl: 'search-location.html',
})
export class SearchLocationPage {
  location : string = "";
  lat : string;
  lng : string;
  searchData : any;
  obsData : Observable<any>;
  
  isLoading : boolean = false;
  pickedData : any;

  constructor(public viewCtrl: ViewController , public gmaps : GoogleGeocodeProvider) {
    
     }
    
  search() {
    this.isLoading = true;
    console.log(this.location);
    this.obsData = this.gmaps.searchLocation(this.location);
    this.obsData.subscribe(data => {
      this.searchData = data.results;
      console.log("RESULT", data.results);
      this.isLoading = false;
      },error => {
         console.log("ERROR",error);
         this.isLoading = false; 
     }); 
}

  pickLocation(data) {
    console.log('picked!');
    this.pickedData = {};
    this.pickedData['location'] = data.formatted_address;
    this.pickedData['lat'] = data.geometry.location.lat;
    this.pickedData['lng'] = data.geometry.location.lng;
    
    this.closeModal();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
  }

  closeModal() {
   this.viewCtrl.dismiss(this.pickedData)
  }

}