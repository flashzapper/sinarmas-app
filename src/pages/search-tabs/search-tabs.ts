import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';
import { SearchProvider } from '../../providers/search/search';

import { DetailProfilePage } from '../../pages/detail-profile/detail-profile';
import { DetailEventPage } from '../../pages/detail-event/detail-event';

import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the SearchTabsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-tabs',
  templateUrl: 'search-tabs.html',
})
export class SearchTabsPage {
  searchKeyword: string;
  selectedSearchTab: string;

  loadingMode:string;
  canLoadMore:boolean;
  selectedRefresher:any;
  selectedLoader:any;
  
  obsData : Observable<any>;
  userData:any;
  eventData:any;

  canEventListLoadMore : boolean
  userLimit=10;
  userOffset=0;
  eventLimit=3;
  eventOffset=0;
  limit=7;
  offset=0;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar : GlobalVarProvider, public searchProv : SearchProvider, public util : UtilityProvider) {
    this.selectedSearchTab = 'username';
    this.selectedLoader = 'more';
    this.canEventListLoadMore = true;
    this.clearSearchBar();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchTabsPage');
  }

  submitSearch(isMore : string)
  {
    console.log('submitSearch()');
    console.log(this.searchKeyword);
    console.log(this.selectedSearchTab);
    switch (this.selectedSearchTab)
    {
      case 'username' : {
        this.searchByUsername(this.searchKeyword);
        break;
      }
      case 'eventname' : {
        this.searchByEventName(this.searchKeyword, isMore);
        break;
      }
      default : {
        this.util.showToast('Select category first.');
        break;
      }
    }
  }

  clearSearchBar()
  {
    this.userData = null;
    this.eventData = null;
  }

  onSearchTabChange($event)
  {
    console.log('onSearchTabChange()');
    this.selectedSearchTab = $event.value;
    console.log(this.selectedSearchTab);
    this.userData = null;
    this.eventData = null;
    this.submitSearch("refresh");
  }

  searchByUsername(username)
  {
    this.obsData = this.searchProv.searchByUsername(username);
    this.obsData.subscribe(response => {
      console.log("response user list", response.data);
      this.userData = (response.data instanceof Array) ? response.data : [response.data];
      if(this.selectedRefresher) this.selectedRefresher.complete();
    },error => {      
      try {
        let errorResponse = JSON.parse(error._body);
        this.util.showToast(errorResponse.message);  
      } catch (exception) {
        this.util.showToast("User tidak ditemukan");  
      }
     
      if(this.selectedRefresher) this.selectedRefresher.complete();
    });
  }

  searchByEventName(eventname : string, isMore : string)
  {
    console.log("event isMore: " + isMore);
    this.eventOffset += this.eventLimit;

    if(isMore == "refresh") 
    {
      this.eventLimit = 3;
      this.eventOffset = 0;
      this.canEventListLoadMore = true;
    }

    this.obsData = this.searchProv.searchByEventName(eventname, this.eventLimit, this.eventOffset);
    this.obsData.subscribe(response => {
      console.log("response event list", response.data);
      if (this.eventData == null)
      {
        this.eventData = [];
      }      
      this.eventData = this.eventData.concat(this.processEventData(response, isMore, this.selectedRefresher));      
      if(this.selectedRefresher) this.selectedRefresher.complete();
    },error => {
      try {
        let errorResponse = JSON.parse(error._body);
        this.util.showToast(errorResponse.message);  
      } catch (exception) {
        this.util.showToast("Event tidak ditemukan"); 
      }
     
      if(this.selectedRefresher) this.selectedRefresher.complete();
    });
  }

  processEventData(response:any, loadingMode:string, $event : any) 
  {
    let data = [];
    if(response.status == 1)
    {		
      if(loadingMode == 'more')
      {
        console.log('more');
        this.canEventListLoadMore = (response.data.length > 0);
        if(response.data.length > 0)
        {
          data = data.concat(response.data);
        }
        else
        {
          console.log('can\'t load more');
        }        
      }
      else
      {	
        data = response.data;          		
      }              
    }
    else
    {
      this.util.showToast(response.message);
    }

    if($event)
    {
      $event.complete();
    }
    
    return data;
  }

  // doUserRefresh($event:any)
  // {
  //   this.selectedRefresher = $event;
  //   this.submitSearch();
  // }

  // doUserLoadMore($event:any)
  // {
  //   this.selectedRefresher = $event;
  //   this.submitSearch();
  // }

  doEventRefresh($event:any)
  {
    this.selectedRefresher = $event;
    this.submitSearch("refresh");
  }

  doEventLoadMore($event:any)
  {
    this.selectedRefresher = $event;
    this.submitSearch("more");
  }

  goToProfilePage(userId)
  {
    this.navCtrl.push(DetailProfilePage, {id : userId});
  }

  goToDetailedEventPage(eventId)
  {
    this.navCtrl.push(DetailEventPage, {id : eventId});
  }
}