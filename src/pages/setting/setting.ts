import { Component } from '@angular/core';
import { IonicPage , App, NavController, NavParams , AlertController } from 'ionic-angular';

import { VerifiedUserPage } from '../verified-user/verified-user';
import { InitPage } from '../init/init';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { GlobalVarProvider } from '../../providers/global-var/global-var';

/**
 * Generated class for the SettingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  myProfile : any;
  tabBarElement : any;

  constructor(private app :App , public navCtrl: NavController, public navParams: NavParams , public globalVar : GlobalVarProvider , public alert : AlertController) {
    this.myProfile = this.globalVar.myProfile;
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }

  logout() {
      this.globalVar.cleanStorage().then((val) =>{
        
        this.globalVar.loginState = false;
        this.globalVar.logoutState = true;
        this.app.getRootNav().setRoot(InitPage);
        
        // this.navCtrl.setRoot(InitPage);
          
          // this.navCtrl.popToRoot().then(()=>{
          //   this.globalVar.loginState = false;
          //   this.globalVar.logoutState = true;
          //   this.navCtrl.setRoot(InitPage);
          //   this.tabBarElement.style.display = "none";
              
          // });
      });
  }

  verifyMe(){
    if(!this.myProfile.submission)
    {
        this.navCtrl.push(VerifiedUserPage, {});
    }
    else
    {
      if(this.myProfile.submission.filing_status == 0 && this.myProfile.is_verified == 0)
      {
          this.showPopUp();
      }
      else if(this.myProfile.submission.filing_status == 1 && this.myProfile.is_verified == 1)
      {
          this.alreadyVerifiedPopup();
      }    
      else 
      {
        this.navCtrl.push(VerifiedUserPage, {});
      }         
    }
  }

  public showPopUp() {
    let alert = this.alert.create({
      title: 'Perhatian!',
      subTitle: 'Permohonan verifikasi user masih diproses , harap cek kembali nanti.',
      buttons: [{
        text: 'OK',
        handler: () => {
          console.log('Clicked OK');
        }
      }]
    });
    alert.present();
  }

  public alreadyVerifiedPopup() {
    let alert = this.alert.create({
      title: 'Perhatian!',
      subTitle: 'Anda sudah menjadi verified user.',
      buttons: [{
        text: 'OK',
        handler: () => {
          console.log('Clicked OK');
        }
      }]
    });
    alert.present();
  }

  public editProfile() {
    this.navCtrl.push(EditProfilePage);
  }

}
