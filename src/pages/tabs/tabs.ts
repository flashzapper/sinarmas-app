import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVarProvider } from '../../providers/global-var/global-var';

import { RefProvider } from '../../providers/ref/ref';
import { UtilityProvider } from '../../providers/utility/utility';
import { ActivityPage } from '../activity/activity';
import { MapsPage } from '../maps/maps';
import { DetailContentPage } from '../detail-content/detail-content';
import { DetailProfilePage } from '../detail-profile/detail-profile';
import { SearchTabsPage } from '../search-tabs/search-tabs';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1 = 'ActivityPage';
  tab2 = 'LocationPage';
  tab3 = 'ProfilePage';
  // tab4 = 'EventPage';
  tab4 = 'EventTabsPage';
  tab5 = 'FriendActivityPage';
  loginState : boolean;
  totalUnreadInboxMessages : number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVar : GlobalVarProvider, public ref : RefProvider, public util : UtilityProvider) {
    this.loginState = this.globalVar.loginState;
    this.getTotalUnreadInboxMessages();
    
    if(this.globalVar.LoginOrRegisterUsingAlert)
    {
      console.log(this.globalVar.lastPage);
      switch(this.globalVar.lastPage)
      {
        case "a" : this.navCtrl.push(ActivityPage,this.globalVar.lastParam); break;
        case "dp" : this.navCtrl.push(DetailContentPage,this.globalVar.lastParam); break;
        case "dc" : this.navCtrl.push(DetailContentPage,this.globalVar.lastParam); break;
        case "m" : this.navCtrl.push(MapsPage,this.globalVar.lastParam); break;
      }      
    }
   }

  getTotalUnreadInboxMessages()
  {
    console.log('getTotalUnreadInboxMessages');
    this.globalVar.getTotalUnreadInboxMessages();

    this.totalUnreadInboxMessages = this.globalVar.totalUnreadInboxMessages;
  }

  resetTotalUnreadInboxMessages()
  {
    this.totalUnreadInboxMessages = 0;
  }
}