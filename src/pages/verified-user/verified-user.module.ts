import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifiedUserPage } from './verified-user';

@NgModule({
  declarations: [
    VerifiedUserPage,
  ],
  imports: [
    IonicPageModule.forChild(VerifiedUserPage),
  ],
})
export class VerifiedUserPageModule {}
