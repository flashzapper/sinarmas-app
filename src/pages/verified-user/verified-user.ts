import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController , ActionSheetController,Platform } from 'ionic-angular';

import { File } from '@ionic-native/file';
import { Transfer ,TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera , CameraOptions } from '@ionic-native/camera';

import { GlobalVarProvider } from '../../providers/global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';
import { UserProvider } from '../../providers/user/user';
import moment from 'moment';
import { Observable } from 'rxjs/Observable';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-verified-user',
  templateUrl: 'verified-user.html',
})
export class VerifiedUserPage {
  myProfile : any;
  obsData : Observable<any>;
  namaLengkap : string;
  noTelp : string;
  tglLahir : string;
  imgDiri : string;
  imgKTP : string;
  mode : number;


  constructor(public navCtrl: NavController, public navParams: NavParams , public globalVar : GlobalVarProvider , public util : UtilityProvider , public alert : AlertController ,private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController ,  public platform: Platform , public user : UserProvider ) {
    this.myProfile = this.globalVar.myProfile;
  }
  pickFoto() {
    this.mode = 1;
    this.presentActionSheet();
  }

  pickKTP() {
    this.mode = 2;
    this.presentActionSheet();
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public getCameraOptions(sourceType) {
    let options : CameraOptions  = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit : true,
      
    };
   
    return options;
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    let options = this.getCameraOptions(sourceType);
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.util.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.util.createFileName());
      }
    }, (err) => {
      this.util.showToast('Error while selecting image.');
    });
  }

  public copyFileToLocalDir(namePath, currentName, newFileName) {
    
        //-- uncomment when deploying to device --//
    
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
          if(this.mode == 1)
            {
              this.imgDiri = newFileName;
            }
            else
            {
              this.imgKTP = newFileName;
            }
        }, error => {
          this.util.showToast('Error while storing file.');
        });
    }

    public pathForImage(img) {
      if (img === null) 
      {
        return '';
      } 
      else 
      {
          //-- uncomment when deploying to device --//       
          return cordova.file.dataDirectory + img;
      }
    }
  

   public submitVerify() {
      // Destination URL
      let url = this.globalVar.urlBase+"/submission_verified";
      // File for Upload
      let targetPath = this.pathForImage(this.imgDiri);
      // File name only
      let filename = this.imgDiri;

      let paramData = {}; 
      paramData['self_image'] = filename;
      paramData['name'] = this.namaLengkap;
      paramData['no_handphone'] = this.noTelp; //@TODO : param baru menyesuaikan dengan service nanti
      paramData['birthday'] = moment(this.tglLahir,"YYYY-MM-DD").format("YYYY-MM-DD HH:mm:ss");
    
      var options = {
        fileKey: "self_image",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : paramData // postData here
      };
      
      options.params['headers'] = { Authorization : "Bearer "+this.globalVar.token};
      console.log(options);    
      const fileTransfer: TransferObject = this.transfer.create();
     
      let loading = this.util.showLoading("Sedang mengupload...")
      loading.present();
     
      // Use the FileTransfer to upload the image
      fileTransfer.upload(targetPath, url, options).then(data => {
          loading.dismissAll()
          console.log("SUKSES Foto Diri",data);
          
          //method upload image ktp
          this.uploadKTP();
        
      }, error => {
          loading.dismissAll()
          console.log("ERROR",error);   
          this.util.showToast(error);
      });
   }
   
   public uploadKTP() {
     // Destination URL
     let url = this.globalVar.urlBase+"/submission_verified/upload-id-card";
     // File for Upload
     let targetPath = this.pathForImage(this.imgKTP);
     // File name only
     let filename = this.imgKTP;

     let paramData = {}; 
     paramData['_method'] = "PUT";
     paramData['image_id_card'] = filename;

     var options = {
       fileKey: "image_id_card",
       fileName: filename,
       chunkedMode: false,
       mimeType: "multipart/form-data",
       params : paramData // postData here
     };
     
     options.params['headers'] = { Authorization : "Bearer "+this.globalVar.token};
     console.log(options);    
     const fileTransfer: TransferObject = this.transfer.create();
    
     let loading = this.util.showLoading("Sedang mengupload...")
     loading.present();
    
     // Use the FileTransfer to upload the image
     fileTransfer.upload(targetPath, url, options).then(data => {
         loading.dismissAll()
         console.log("SUKSES Foto KTP",data);
         this.alertSukses();

     }, error => {
         loading.dismissAll()
         console.log("ERROR",error);   
         this.util.showToast(error);
     });
   }

   public alertSukses() {
    let alert = this.alert.create({
      title: 'Sukses',
      subTitle: 'Permohonan verifikasi sedang diproses. Cek secara berkala status verify user anda ke menu ini.',
      buttons: [{
        text: 'OK',
        handler: () => {
          console.log('Clicked OK');
          this.navCtrl.pop();
        }
      }]
    });
    alert.present();
  }

  public showPopUp() {
    let alert = this.alert.create({
      title: 'Perhatian!',
      subTitle: 'Permohonan verifikasi user ditolak, silahkan submit ulang dengan data yang benar.',
      buttons: [{
        text: 'OK',
        handler: () => {
          console.log('Clicked OK');
          this.changeFillingStatus();
          //TODO : balikan status menjadi 0
        }
      }]
    });
    alert.present();
  }

  changeFillingStatus() {
    let paramData = {};
    this.obsData  = this.user.updateFillingStatus(paramData);
    this.obsData.subscribe(response => {
      console.log("RESULT : ",response);
      this.myProfile.submission.filing_status = 0;    
  },error => {
    this.util.showToast(error);
  })
  }

 
  ionViewDidEnter() {
    if(this.myProfile.submission)
      {
        if(this.myProfile.submission.filing_status == 2)
          {
              this.showPopUp();
          }
      }
  
  }

}
