import { Injectable } from '@angular/core';
import { Http,RequestOptions } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';

/*
  Generated class for the ActivityProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

@Injectable()
export class ActivityProvider {

  constructor(public http: Http ,  public globalVar : GlobalVarProvider , public nativeHttp : HTTP) {
    console.log('Hello ActivityProvider Provider');
  }

    getActivity(limit,offset , mode) {
    let urlEndpoint;
    if(mode == "ALL")
    {
       urlEndpoint = this.globalVar.urlBase + "/posting?limit="+limit+"&offset="+offset;
       return this.http.get(urlEndpoint).map(res => res.json());
    }
    else
    {
      let headers = this.globalVar.getDefaultHeader();
      let options = new RequestOptions({ headers: headers });
      urlEndpoint = this.globalVar.urlBase + "/posting/friend?limit="+limit+"&offset="+offset;
      return this.http.get(urlEndpoint,options).map(res => res.json());
    }
    
   
  }

  detailActivity(id) {
    let urlEndpoint = this.globalVar.urlBase + "/posting/"+id;
    return this.http.get(urlEndpoint).map(res => res.json());
  }

  postActivity() {
    let urlEndpoint = this.globalVar.urlBase + "/posting";
  }

}
