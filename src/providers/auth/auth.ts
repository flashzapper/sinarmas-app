import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AuthProvider {

  constructor(public http: Http ,  public globalVar : GlobalVarProvider) {
  
  }

  register(data) {
    let urlEndpoint = this.globalVar.urlBase + "/register";
    
    return this.http.post(urlEndpoint,data).map(res => res.json());
  }

  login(data) {
    let urlEndpoint = this.globalVar.urlBase + "/login";
    //let header = this.globalVar.getDefaultHeader();
    
    return this.http.post(urlEndpoint,data).map(res => res.json());
  }

  

}
