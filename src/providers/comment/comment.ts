import { Injectable } from '@angular/core';
import { Http ,RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';

/*
  Generated class for the CommentProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CommentProvider {

  constructor(public http: Http ,  public globalVar : GlobalVarProvider) {
   
  }

  postComment(data) {
    let urlEndpoint = this.globalVar.urlBase + "/comment";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });
    return this.http.post(urlEndpoint,data,options).map(res => res.json());
  }

  getComment(id_activity , limit , offset) {
    let urlEndpoint = this.globalVar.urlBase + "/getcomments/"+id_activity+"/?limit="+limit+"&offset="+offset;
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });
    return this.http.get(urlEndpoint,options).map(res => res.json());
  }

}
