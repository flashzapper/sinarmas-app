import { Injectable } from '@angular/core';
import { Http ,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';

/*
  Generated class for the EventsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class EventsProvider {

  constructor(public http: Http, public globalVar : GlobalVarProvider) {
   }

  events() {
    let urlEndpoint = this.globalVar.urlBase + "/events/by-date?is_approved=1";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint,options).map(res => res.json());
  }

  // date format: [Y-M-D hh:mm:ss]
  getEventsByDate(startDate, endDate) {
    let urlEndpoint = this.globalVar.urlBase + "/events/by-date?start_date="+ startDate +"&end_date="+ endDate +"&is_approved=1";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint,options).map(res => res.json());
  }

  detailEvents(id_event) {
    let urlEndpoint = this.globalVar.urlBase + "/events/"+id_event+"/details";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint,options).map(res => res.json());
  }

  createEvents(data) {
    let urlEndpoint = this.globalVar.urlBase + "/events";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.post(urlEndpoint,data,options).map(res => res.json());
  }

  attendEvents(id_event) {
    let urlEndpoint = this.globalVar.urlBase + "/events/"+id_event+"/attend";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.put(urlEndpoint,null,options).map(res => res.json());
  }

  unattendEvents(id_event) {
    let urlEndpoint = this.globalVar.urlBase + "/events/"+id_event+"/not-attending";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.put(urlEndpoint,null,options).map(res => res.json());
  }

  donateSeed(eventId, userId, numberOfSeeds, treeId) {
    console.log('donateSeed()');
    let data = {
      // user_id: userId, // sudah terwakili oleh token
      number_of_seeds: numberOfSeeds,
      tree_id: treeId
    };
    let urlEndpoint = this.globalVar.urlBase + "/events/"+eventId+"/donate_seed";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    console.log('urlEndpoint: ' + urlEndpoint);
    console.log('data: ', data);
    return this.http.post(urlEndpoint, data, options).map(res => res.json());
  }

  getEventsWithNoSponsor() {
    let urlEndpoint = this.globalVar.urlBase + "/event/no-sponsor";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint,options).map(res => res.json());
  }  

  getParticipants(eventId, userType, limit, offset) {
    // participants
    let urlEndpoint = this.globalVar.urlBase + "/events/"+eventId+"/"+userType+"?limit="+limit+"&offset="+offset;
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint,options).map(res => res.json());
  }

  searchParticipants(eventId, keyword, userType, limit, offset) {
    let data = {
      keyword: keyword,
      limit: limit,
      offset: offset
    };

    let urlEndpoint = this.globalVar.urlBase + "/events/"+eventId+"/"+userType+"/search";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.post(urlEndpoint, data, options).map(res => res.json());
  }
}