import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { File } from '@ionic-native/file';
import { Transfer ,TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';

import { UtilityProvider } from '../utility/utility';

/*
  Generated class for the FileTransferProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

  

@Injectable()
export class FileTransferProvider {
  lastImage: string = null;

  constructor(public http: Http ,public transfer: Transfer, public file: File, public filePath: FilePath , public util : UtilityProvider) {
  
  }

 
// Always get the accurate path to your apps folder
 

}
