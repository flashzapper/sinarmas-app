import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
/*
  Generated class for the GlobalVarProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class GlobalVarProvider {
  public loginState : boolean = false;
  public logoutState : boolean = false;
   public urlBase : string = "http://139.59.121.185/api";
  // public urlBase : string = "http://192.168.0.101/git/labtekindie/sinarmas-api/api";
  // public urlBase : string = "http://localhost/git/labtekindie/sinarmas-api/api";
  public token : string;
  public isRegistered : number;
  public headers : any;
  public limit : number = 10;
  public offset : number = 0;
  public myProfile : any;
  public lastPage : string;
  public lastParam : any;
  public LoginOrRegisterUsingAlert : boolean = false;

  public isAlertPresent : boolean = false;

  public totalUnreadInboxMessages : number = 0;

  constructor(public http: Http , public storage : Storage){
  }

  getToken() { 
    return this.token;      
  }

  setToken(token:string) { 
    this.storage.set('token', token);
    this.token = token;
  }

  setRegistered(val:number) {
    this.storage.set('isRegistered', val);
    this.isRegistered = val;
  }

  cleanStorage() {
    return this.storage.clear();
  }

  getDefaultHeader () {
      let headers = new Headers();
      headers.append('Authorization', 'Bearer '+this.token);  
      // console.log('token: ', this.token);
      return headers;
  }

  getTotalUnreadInboxMessages()
  {
    let urlEndpoint = this.urlBase + "/seed-event/inbox/count-unread-messages";
    let headers = this.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    let obsData = this.http.get(urlEndpoint, options).map(res => res.json());
    obsData.subscribe(response => {
      this.totalUnreadInboxMessages = response.total_messages;
      console.log(this.totalUnreadInboxMessages);
    }, error => {
      this.resetTotalUnreadInboxMessages();
      // this.util.showToast(error);
    });
  }

  resetTotalUnreadInboxMessages()
  {
    this.totalUnreadInboxMessages = 0;
    console.log(this.totalUnreadInboxMessages);
  }
}