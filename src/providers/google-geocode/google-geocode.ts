import { Injectable, ElementRef } from '@angular/core';
import { Http ,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';
import { GoogleMaps, GoogleMap, GoogleMapsEvent , GoogleMapOptions , LatLng} from "@ionic-native/google-maps";



/*
  Generated class for the GoogleGeocodeProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class GoogleGeocodeProvider {
  map: GoogleMap;

  constructor(public http: Http , public globalVar : GlobalVarProvider , private googleMaps: GoogleMaps) {
  }
    searchLocation(query) {
      let urlEndpoint = "http://maps.google.com/maps/api/geocode/json?address="+encodeURIComponent(query)+"&components=country:ID";
      // let urlEndpoint = "http://maps.google.com/maps/api/geocode/json?address="+encodeURIComponent(query)+"&components=country:ID";
      return this.http.get(urlEndpoint).map(res => res.json());
    }

    searchLocationByLatLng(lat,lng) {
      let urlEndpoint = "http://maps.google.com/maps/api/geocode/json?latlng="+lat+","+lng;
      // let urlEndpoint = "http://maps.google.com/maps/api/geocode/json?address="+encodeURIComponent(query)+"&components=country:ID";
      return this.http.get(urlEndpoint).map(res => res.json());
    }

    setDiv(el: HTMLElement): Promise<any> {
      return new Promise<any>((resolve) => {
        if(!this.map && el != null){
          let baseLat = "-0.789275";
          let baseLng = "113.921327";
          let mapOptions: GoogleMapOptions = {
            camera: {
              target: {
                lat: baseLat,
                lng: baseLng
              },
              zoom: 4,
              tilt: 30
            }
          };

          this.map = this.googleMaps.create(el,mapOptions);
          this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
            console.log("MAPS IS READY");
            resolve();
          });
        } else {
      

          this.map.clear();
          this.map.setDiv(el);
          let baseLat = -0.789275;
          let baseLng = 113.921327;
          let zoom = 3, tilt: 30;
          this.map.setCameraTarget(new LatLng(baseLat,baseLng))
          this.map.setCameraZoom(zoom);
          this.map.setCameraTilt(tilt);

          resolve();
        }
      });
    }
  
  
  

}
