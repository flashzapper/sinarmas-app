import { Injectable } from '@angular/core';
import { Http ,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';


/*
  Generated class for the InboxProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class InboxProvider {

  constructor(public http: Http, public globalVar : GlobalVarProvider) {
    console.log('Hello InboxProvider Provider');
  }

  ionViewCanLeave()
  {
    console.log('onBackButtonPressed');
    this.globalVar.getTotalUnreadInboxMessages();
  }

  getSeedDonationInboxMessages(limit, offset) 
  {
    let urlEndpoint = this.globalVar.urlBase + "/seed-event/inbox?limit="+limit+"&offset="+offset;
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint, options).map(res => res.json());
  }

  searchSeedDonationInboxMessages(keyword, limit, offset)
  {
    let data = {
      keyword: keyword,
      limit: limit,
      offset: offset
    };

    let urlEndpoint = this.globalVar.urlBase + "/seed-event/inbox/search";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.post(urlEndpoint, data, options).map(res => res.json());
  }  

  readSeedDonationInboxMessage(seedEventApprovalId)
  {
    let urlEndpoint = this.globalVar.urlBase + "/seed-event/inbox/"+seedEventApprovalId+"/read";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.post(urlEndpoint, {}, options).map(res => res.json());
  }  
  
  approveDonation(seedEventApprovalId) 
  {
    let urlEndpoint = this.globalVar.urlBase + "/seed-event/inbox/"+seedEventApprovalId+"/approve_donation";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.post(urlEndpoint, {}, options).map(res => res.json());
  }
  
  rejectDonation(seedEventApprovalId) 
  {
    let urlEndpoint = this.globalVar.urlBase + "/seed-event/inbox/"+seedEventApprovalId+"/reject_donation";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.post(urlEndpoint, {}, options).map(res => res.json());
  }
}