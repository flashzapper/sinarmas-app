import { Injectable } from '@angular/core';
import { Http ,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';
import { UtilityProvider } from '../../providers/utility/utility';

// for handling upload image
import { Transfer ,TransferObject } from '@ionic-native/transfer';
declare var cordova: any;

/*
  Generated class for the RefProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class RefProvider {

  constructor(public http: Http ,  public globalVar : GlobalVarProvider, public util : UtilityProvider, private transfer: Transfer) {
    console.log('Hello RefProvider Provider');
  }

  trees() {
    let urlEndpoint = this.globalVar.urlBase + "/trees";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });
    
    return this.http.get(urlEndpoint,options).map(res => res.json());
  }

  events() {
    let urlEndpoint = this.globalVar.urlBase + "/events?is_approved=1&&order_type=asc&&order_by=id";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint,options).map(res => res.json());
  }

  sponsor() {
    let urlEndpoint = this.globalVar.urlBase + "/sponsor";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint,options).map(res => res.json());
  }
  
  // methods for uploading generic image 
  // end point: http://url.com/api/upload_image (POST) 
  // with request body: { "image":"[multipart]" }
  // response: { "file_name":"http://url.com/image.jpg" }
  // how to use:
  //  this.ref.uploadImage(imageFileNameFromCamera/Gallery)
  //      .then(result => {    // the method "then()" is the callback method to handle the API response;
  //        // put the algorithm here
  //        console.log("SUKSES upload gambar", result);
  //        let apiResponse = JSON.parse(result.response);
  //        if (apiResponse.data)
  //        {
  //          resultImageURL = apiResponse.data.file_name;
  //        }  
  //      });
  uploadImage(imageFileName)
  {
    
    let url = this.globalVar.urlBase+"/upload_image"; // Destination URL
    let targetPath = this.pathForImage(imageFileName); // File for Upload

    let paramData = {}; 
    paramData['_method'] = "POST";
    paramData['image'] = targetPath;

    var options = {
      fileKey: "image",
      fileName: targetPath,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : paramData // postData here
    };
    
    options.params['headers'] = { Authorization : "Bearer "+this.globalVar.token};

    // Use the FileTransfer to upload the image
    // return: the callback "Promise<FileUploadResult>"
    const fileTransfer: TransferObject = this.transfer.create();   
    return fileTransfer.upload(targetPath, url, options);
  }

  public pathForImage(img) 
  {
    if (img === null) 
    {
      return '';
    } 
    else 
    {
        //-- uncomment when deploying to device --//       
        return cordova.file.dataDirectory + img;
    }
  }  
}