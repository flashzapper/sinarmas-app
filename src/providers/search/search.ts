import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';

/*
  Generated class for the SearchProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SearchProvider {

  constructor(public http: Http, public globalVar : GlobalVarProvider) {
    console.log('Hello SearchProvider Provider');
  }

  searchByUsername(username) {
    let urlEndpoint = this.globalVar.urlBase + "/search/by-username";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    let data = { username : username };

    return this.http.post(urlEndpoint,data,options).map(res => res.json());
  }

  searchByEventName(eventname, limit, offset) {
    let urlEndpoint = this.globalVar.urlBase + "/search/by-eventname";
     let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    let data = { 
      eventname : eventname,
      limit : limit,
      offset : offset
     };

    return this.http.post(urlEndpoint,data,options).map(res => res.json());
  }
}