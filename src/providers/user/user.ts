import { Injectable } from '@angular/core';
import { Http,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVarProvider } from '../global-var/global-var';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: Http , public globalVar : GlobalVarProvider) {
    console.log('Hello UserProvider Provider');
  }

  getMyProfile() {
      let urlEndpoint = this.globalVar.urlBase + "/myprofile";
      let headers = this.globalVar.getDefaultHeader();
      let options = new RequestOptions({ headers: headers });

      return this.http.get(urlEndpoint,options).map(res => res.json());
  }

  getOtherProfile(id:number) {
    let urlEndpoint = this.globalVar.urlBase + "/profile/"+id;
    
    return this.http.get(urlEndpoint).map(res => res.json());
  }

  updateProfile(data:any) {
    let urlEndpoint = this.globalVar.urlBase + "/myprofile";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });
    
    return this.http.post(urlEndpoint,data,options).map(res => res.json());
  }
  
  updateFillingStatus(data) {
    let urlEndpoint = this.globalVar.urlBase + "/re-submission";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.put(urlEndpoint,data,options).map(res => res.json());
  }

  addFriend(data) {
    let urlEndpoint = this.globalVar.urlBase + "/follow";
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.post(urlEndpoint,data,options).map(res => res.json());
  }

  checkFriend(profile_id) {
    let urlEndpoint = this.globalVar.urlBase + "/is_following/"+profile_id;
    let headers = this.globalVar.getDefaultHeader();
    let options = new RequestOptions({ headers: headers });

    return this.http.get(urlEndpoint,options).map(res => res.json());
  }
      

}
