import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { ToastController , LoadingController , AlertController , ModalController} from 'ionic-angular';
import moment from 'moment';

import { LoginPage } from '../../pages/login/login';
import { RegisterPage } from '../../pages/register/register';
import { ActivityPage } from '../../pages/activity/activity';



import { GlobalVarProvider } from '../../providers/global-var/global-var';

/*
  Generated class for the UtilityProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/


@Injectable()
export class UtilityProvider {


  constructor(public toastCtrl : ToastController , public loading : LoadingController , public alertCtrl : AlertController ,  public modal : ModalController, public globalVar : GlobalVarProvider ) {
  }

  showToast(text:string) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      showCloseButton : true
   });
   toast.present();


    // toast.onDidDismiss(() => {
    //   this.navCtrl.setRoot(TabsPage);
    // });

  }

   // Create a new name for the image
createFileName() {
  let d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}

showLoading (text:string) {
  let loading = this.loading.create({
    content : text
  });

  return loading;
}

formattedDate(datetime) {
  let formatted;
  let now = moment(new Date()); //todays date
  let end = moment(datetime); // another date
  let duration = moment.duration(now.diff(end));

  let days = duration.asDays();
  let hours = duration.asHours();
  let minutes = duration.asMinutes();
  let time;
  let ext;

  if(Math.floor(days) <= 0)
    {
    if(Math.floor(hours) <= 0)
        {
            time = minutes;
            ext = " m ago";
        }
        else
        {
            time = hours;
            ext = " h ago";
        }
    }
    else
    {
        time = days;
        ext = " d ago";
    }

    time = Math.floor(time);
    formatted = time + ext;
    
  return formatted;
}

alertGuest (lastPage : any , lastParam : any) {
  console.log("LASTPAGE",lastPage);
  console.log("LASTPARAM",lastParam);

  this.globalVar.lastPage = lastPage;
  this.globalVar.lastParam = lastParam;

  if(!this.globalVar.isAlertPresent)
  {
    this.globalVar.isAlertPresent = true;

    const alert = this.alertCtrl.create({
      title: 'Halo!',
      message: 'Mari bergabung dengan Foresocio untuk menjadikan bumi lebih baik bersama-sama! <br><br> Daftar Sekarang !',
      //subTitle : 'Daftar Sekarang !',
      buttons: [
        {
          text: 'Masuk',
          //role: 'cancel',
          handler: () => {
            this.globalVar.isAlertPresent = false;
            this.globalVar.LoginOrRegisterUsingAlert  = true;
            
            console.log('go to Login Page');
            let modal = this.modal.create(LoginPage);
            modal.onDidDismiss(data => {
              console.log(data);
            });
            modal.present();
            
          }
        },
        {
          text: 'Daftar',
          handler: () => {
            this.globalVar.isAlertPresent = false;
            this.globalVar.LoginOrRegisterUsingAlert  = true;
            console.log('go to Register Page');
            let modal = this.modal.create(RegisterPage);
            modal.onDidDismiss(data => {
              console.log(data);
            });
            modal.present();
          }
        }
      ]
    });
    alert.onDidDismiss(() => {
      this.globalVar.isAlertPresent = false;
    });

    alert.present();

  }
  
}

showAlertImageTooLarge() {
  const alert = this.alertCtrl.create({
    title: 'Ups!',
    message: 'Gambar milikmu terlalu besar.',
    buttons :[
      {
        text : "Upload Gambar Lainnya",
        handler: () => {}
      }
    
    ]
    
  });
  alert.onDidDismiss(() => {
    
  });

  alert.present();
  //return alert;
}

checkAvatar(avatar) {
  if(avatar)
  {
    let url = "";
    return url + avatar;
  }
  else
  {
    return "assets/img/user-placeholder.png";
  }
}

latinDateTime(val) {
  let concatDateTime = val;
  let check = moment(concatDateTime, 'YYYY-MM-DD');    
  let format = check.lang("id").format("DD MMM YYYY");
  return format;
}

// public getPageReference(pageId: number): any {
//   switch(pageId) {
//     case 1: return ActivityPage;
//   }
// }




}
